#!/bin/bash

#SBATCH --job-name=bowtie2.L.mapping
#SBATCH --nodes=1
#SBATCH --ntasks=32
#SBATCH --mem=125G
#SBATCH --partition=normal

#SBATCH --error="/home/kegravou/COMPARATIF-BINNING/log/bowtie2.VS.%J.err"
#SBATCH --output="/home/kegravou/COMPARATIF-BINNING/log/bowtie2.VS.%J.out"

#SBATCH --mail-user=kevin.gravouil@udamail.fr
#SBATCH --mail-type=ALL



#--- usage ---
#
# sbatch map-bowtie2-PE.sh <reference.fasta> <reads_1.fastq.gz> <reads_2.fastq.gz>
#
#--- reads on scaffolds ---
#
# sbatch scripts/map-bowtie2-PE.sh results/ray.L/Scaffolds.fasta data/L.reads-reads_1.fastq.gz data/L.reads-reads_2.fastq.gz
# sbatch scripts/map-bowtie2-PE.sh results/ray.M/Scaffolds.fasta data/M.reads-reads_1.fastq.gz data/M.reads-reads_2.fastq.gz
# sbatch scripts/map-bowtie2-PE.sh results/ray.S/Scaffolds.fasta data/S.reads-reads_1.fastq.gz data/S.reads-reads_2.fastq.gz
#


if [[ $# -ne 3 || ! -f $1 || ! -f $2 || ! -f $3 ]] ; then
	echo "not enough arguments"
	echo "# usage: sbatch map-bowtie2-PE.sh <reference.fasta> <reads_1.fastq.gz> <reads_2.fastq.gz>"
	exit 1;
fi;


echo "Hello! [$(date +"%Y-%m-%d %T")]"
echo "JOB ID=$SLURM_JOB_ID"

which bowtie2-build
bowtie2-build --usage

which bowtie2
bowtie2 --help

#--- variables ---

WDIR="/storage/scratch/kegravou/$SLURM_JOB_ID"
mkdir -p $WDIR

REF=$1
READS1=$2
READS2=$3
THREADS=8

EXPERIM=$(basename $READS1 .reads-reads_1.fastq.gz)
REFIDX="${EXPERIM}.$(basename $REF).idx"

OUTDIR="$WDIR/bowtie2.${EXPERIM}.very-sensitive"
mkdir -p $OUTDIR

OUTSAM="$OUTDIR/${EXPERIM}.$(basename $REF .fasta).sam"
OUTBAM="$OUTDIR/${EXPERIM}.$(basename $REF .fasta).bam"



#--- processing ---

echo "# copying files [$(date +"%Y-%m-%d %T")]"
cp $REF $WDIR &
cp $READS1 $WDIR &
cp $READS2 $WDIR &
wait

# update file paths
REF=$WDIR/$(basename $REF)
REFIDX=$WDIR/$REFIDX
READS1=$WDIR/$(basename $READS1)
READS2=$WDIR/$(basename $READS2)

ls -lah $WDIR
echo "copy done"



echo "# indexing scaffolds [$(date +"%Y-%m-%d %T")]"

bowtie2-build --threads $THREADS $REF $REFIDX

echo "index done"


echo "# running mapping [$(date +"%Y-%m-%d %T")]"

# mkfifo file1.fifo
# mkfifo file2.fifo
# gunzip $READS1 > file1.fifo &
# gunzip $READS2 > file2.fifo &
bowtie2 --threads $THREADS --time \
  -x $REFIDX -1 $READS1 -2 $READS2 \
  -S $OUTSAM \
  --end-to-end --very-sensitive

echo "mapping done"


echo "# converting SAM to BAM [$(date +"%Y-%m-%d %T")]"
samtools view --threads $THREADS -S -b $OUTSAM > $OUTBAM
rm $OUTSAM
md5sum $OUTBAM > $OUTBAM.md5
echo "conversion done"

ls -lah $OUTDIR

echo "# collecting results [$(date +"%Y-%m-%d %T")]"
cp -r $OUTDIR $HOME/COMPARATIF-BINNING/results/
echo "collecting done"

echo "# cleaning [$(date +"%Y-%m-%d %T")]"
rm -Rf $WDIR
echo "cleaning done"

echo "Bye. [$(date +"%Y-%m-%d %T")]"
exit;
