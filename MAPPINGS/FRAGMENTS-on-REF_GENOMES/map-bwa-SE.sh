#!/usr/bin/env bash

#SBATCH --job-name=bwamem.mapping
#SBATCH --nodes=1
#SBATCH --ntasks=32
#SBATCH --mem=80G
#SBATCH --partition=debug

#SBATCH --error="/home/kegravou/COMPARATIF-BINNING/MAP/log/bwamem.%J.err"
#SBATCH --output="/home/kegravou/COMPARATIF-BINNING/MAP/log/bwamem.%J.out"

#SBATCH --mail-user=kevin.gravouil@udamail.fr
#SBATCH --mail-type=ALL



#--- usage ---
#
# sbatch map-bwa-SE.sh <reference.fasta> <scaffolds.fasta[.gz]>
#
#-- map cut up scaffolds (c10k) on reference genomes
#
# sbatch scripts/map-bwa-SE.sh ref_genomes/S.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/S.Scaffolds.1kb+.split10kb.fasta
# sbatch scripts/map-bwa-SE.sh ref_genomes/M.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/M.Scaffolds.1kb+.split10kb.fasta
# sbatch scripts/map-bwa-SE.sh ref_genomes/L.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/L.Scaffolds.1kb+.split10kb.fasta
#
# bash scripts/map-bwa-SE.sh DATA-SIMULATION/genomes.S/S.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/S.Scaffolds.1kb+.split10kb.fasta
# bash scripts/map-bwa-SE.sh DATA-SIMULATION/genomes.M/M.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/M.Scaffolds.1kb+.split10kb.fasta
# bash scripts/map-bwa-SE.sh DATA-SIMULATION/genomes.L/L.ALL.GENOMES.fasta CUT-UP-SCAFFOLDS/L.Scaffolds.1kb+.split10kb.fasta


set -x

if [[ $# -ne 2 || ! -f $1 || ! -f $2 ]] ; then
	echo "not enough arguments"
	echo "# usage: sbatch map-bwa-SE.sh <reference.fasta> <scaffolds.fasta>"
	exit 1;
fi;


echo "Hello! [$(date +"%Y-%m-%d %T")]"
echo "JOB ID=$SLURM_JOB_ID"

command -v bwa
command -v samtools

#--- variables ---
THREADS=32
TIME="/usr/bin/time -v "

WDIR="/storage/scratch/kegravou/$SLURM_JOB_ID"
mkdir -p $WDIR

REF=$1
READS1=$2

EXPERIM=$(basename $REF .ALL.GENOMES.fasta)
REFIDX="$(basename $REF).idx"

OUTDIR="$WDIR/bwamem_SCAFFOLDS.1kb+.split10kb_${EXPERIM}.ALL"
mkdir -p $OUTDIR

OUTSAM="$OUTDIR/$(basename $REF .fasta).sam"
OUTBAM="$OUTDIR/$(basename $REF .fasta).bam"



#--- processing ---

echo "# copying files [$(date +"%Y-%m-%d %T")]"
cp $REF $WDIR &
cp $READS1 $WDIR &
wait

# update file paths
REF=$WDIR/$(basename $REF)
REFIDX=$WDIR/$REFIDX
READS1=$WDIR/$(basename $READS1)

ls -Rlah $WDIR
echo "copy done"



echo "# indexing genomes [$(date +"%Y-%m-%d %T")]"

$TIME bwa index $REF -p $REFIDX 2>&1

echo "index done"


echo "# running mapping [$(date +"%Y-%m-%d %T")]"

# Usage: bwa mem [options] <idxbase> <in1.fq> [<in2.fq>]

$TIME bwa mem \
  -t $THREADS \
  -x intractg \
  $REFIDX \
  $READS1 \
  > $OUTSAM


echo "mapping done"


echo "# converting SAM to BAM [$(date +"%Y-%m-%d %T")]"
samtools view -@ $THREADS -S -b $OUTSAM > $OUTBAM
rm $OUTSAM
md5sum $OUTBAM > $OUTBAM.md5

ls -Rlah $OUTDIR

echo "conversion done"


echo "# collecting results [$(date +"%Y-%m-%d %T")]"
cp -r $OUTDIR $HOME/COMPARATIF-BINNING/MAP/
echo "collecting done"

echo "# cleaning [$(date +"%Y-%m-%d %T")]"
# rm -Rf $WDIR
echo "cleaning done"

echo "# Bye. [$(date +"%Y-%m-%d %T")]"
exit;
