# Comparison of unsupervised metagenomic binning software

This document describe the file architecture of this respository.

Most of the files were compressed using `xz`. You may uncrompressed them all using:

```bash
find . -type f -name "*xz" | xargs -I % unxz %
```

You may recompress them using:

```bash
find . -type f -size +10M ! -name "*.xz" | xargs -I % xz %
```

> Considering the Gitlab repository size limitation, as few as possible datasets are present (especially mappings). Instructions to create these files are given below.


## `DATA-SIMULATION/`

### Scripts

- `update_ncbi_taxo.sh` download the latest NCBI taxonomy database to `ncbi_taxdb/`
- `getRefGenomes.sh` chooses genomes from RefSeq then download them
- `GetCompleteLineage.pl` retrieves the complete lineage from a given taxonomic id
- `run-grinder.sh` run Grinder to simulate reads from the reference genomes

> ⚠ Grinder may take some time.

### Outputs

- `genomes.{S,M,L}/{S,M,L}.reads-ranks.txt` contains the real abundances of each contig
- `genomes.{S,M,L}/{S,M,L}.reads-reads.fastq` contains the paired-ends reads
- `genomes.{S,M,L}/{S,M,L}.reads-reads_{1,2}.fastq` separate forward and reversed pair reads

## `ASSEMBLY/`

Assembly was run in a SLURM environment at the [CRRI](https://crri.clermont-universite.fr/). The following script must be adapted to your SLURM environment and submitted using `sbatch slurm-script.sh`.

### Scripts

- `slurm.assembly-ray.S.sh` run the Ray meta assembly in SLURM.

### Outputs

- `ray.{S,M,L}/` folders which contains a `Scaffolds.fasta` file (+ others info)

## `FILTER-BY-LENGTH/`

### Scripts

- `filterByLength.pl 1000 ray.scaffolds.fasta > ray.scaffolds.1kb+.fasta`

### Outputs

- `ray.{S,M,L}/Scaffolds.1kb+.fasta` contains only sequences longer than 1 kb

## `CUT-UP-SCAFFOLDS/`

### Scripts

This script is the python3 version of [CONCOCT's](https://github.com/BinPro/CONCOCT/blob/master/scripts/cut_up_fasta.py)

- `cut_up_fasta.py -c 10000 -o 0 -m Scaffolds.1kb+.fasta > Scaffolds.1kb+.split10kb.fasta`

### Outputs

- `{S,M,L}.Scaffolds.1kb+.split10kb.fasta` contains filtered and cut up scaffolds to be binned.

## `MAPPINGS/`

### READS-on-FRAGMENTS/

Simulated reads were mapped on fragmented sequences to provide sequence abundance information to the binning tools using the `map-bowtie2-PE.sh` script.
Mapping was done using bowtie2 version 2.2.8 with parameters `--end-to-end` `--very-sensitive`.
The mapping was done in a SLURM environment by running `sbatch map-bowtie2-PE.sh <reference.fasta> <reads_1.fastq[.gz]> <reads_2.fastq[.gz]`.

### FRAGMENTS-on-REF_GENOMES/

Reference bins are built by mapping fragmented sequences on reference genomes using BWA mem version 0.7.15‑r1140 using the `-x intractg` parameter set ("intra-species contigs to ref").
The mapping was also run in a SLURM environment using `sbatch reference.fasta> <scaffolds.fasta[.gz]>`.

## `BINNING/`

Each subfolder contains a `run.sh` script which:

- set up the Conda environment
- check things up
- preprocess and process the cut up filtered scaffolds

If the `run.sh` needs parameters to run correctly (such as path to the input data), it will tell you. These scripts monitor the execution using [GNU time](https://www.gnu.org/software/time/) (`/usr/bin/time`) and save the results to the results in a `log/` subfolder.


The `CAMI_formatted_results/` folder is made from the AMBER raw results and formatted to integrate into the pipeline (which mostly involve renaming files).

The `CAMI_raw_results/` folder contains the original CAMI results. The `cami-command.sh` script contains the AMBER command to produce these results and command lines to get the `RESULTS-ANALYSIS/CAMI1h.scaffold-taxid.tsv` file (see below).

## `RESULTS-ANALYSIS/`

### Scripts

- `MAIN.sh` is the main script which calls all the others.
- `format-outputs.sh` formats binnings outpus in TSV files
- `add-unbinned.pl` adds unbinned sequences if needed to binning results
- `tsv2binning` converts TSV binning file to Bioboxes binning format
- `classes-clusters-association.R` maps reference bins to proposed bins
- `plot-binning-metrics.R` plot average precision and average accuracy of all tool in one scatter plot.
- `check-taxo.pl` retrieves complete lineage for each binned sequences and computes the binning results profile for each tool
- `get_percgoodclassif.R` computes the percentage of good classification per taxa using the `diffs/` files
- `binning.results.per.phylum.R` produces the violin plots from the results of `get_percgoodclassif.R` and saves them to `TAXONOMY/`
- `cmp.binner.pcoa.R` compares binning profiles using a PCoA and plot the 2D visualisation to `PCOA/`

`MAIN.sh` can be parametrized using bash environment variables. It default behaviour is:

```bash
SKIP_FORMAT=0 WITH_UNBINNED=1 PACKAGE_RESULTS=1 FMT_BINNING=0 PURGE=0 nice ./MAIN.sh
```

- `SKIP_FORMAT=1` will not run the `format-outputs.sh`
- `WITH_UNBINNED=0` will not consider unbinned sequences in the metric calculation
- `PACKAGE_RESULTS=1` will move all results to either `RESULTS_WITHOUT_UNBINNED/` or `RESULTS_WITH_UNBINNED/` depending on `WITH_UNBINNED` value.
- `FMT_BINNING=1` will additionanly format the binning results to [bioboxes `.binning`](https://github.com/bioboxes/rfc/blob/4bb19a633a6a969c2332f1f298852114c5f89b1b/data-format/binning.mkd) files.
- `PURGE=1` will delete all the generated files (not the scripts) unless they are packaged, then exit.

### Notes

- ⚠ __Take care of the working directory (`$WD`) in `MAIN.sh` and `format-outputs.sh`. You will need to change it.__ ⚠

- `MAIN.sh` run mutliple scripts in background. I recommend the use of `nice ./MAIN.sh`.

- The `legacy-scripts/` contains some unused scripts.

### Outputs

The output files will be in the current directory or the packaging directory (see `PACKAGE_RESULTS`). Following files will be produced:

- `{S,M,L,CAMI1h}.scaffold-ids.txt` is the complete list of sequence identifiers for each dataset
- `CAMI1h.scaffold-taxid.tsv` is the database linking sequence identifiers to taxonomic identifiers (see `BINNING/CAMI_raw_results/cami-command.sh`)
- `data/` contains binning results (TSV-formatted)
- `data-with-unbinned/` contains binning results with unbinned sequences, if needed
- `assoc/` contains association files between reference bins (classes) and proposed bins (clusters)
- `heatmaps/` contains the classes-clusters association heatmap
- `stats/` contains evaluation metrics (one file per software)
- `TAXONOMY/` contains taxonomic analysis figures
- `PCOA/` contains binning results diversity analysis figures
- `diffs/` contains complementary analysis tables (+ lineage for each sequence)
- `stats-all.csv` is the concatenation of `stats/` files
- `precision-accuracy-plot.svg` and `precision-accuracy-plot.png` are the precision/accuray scatter plots, scaled from 0 to 1 on both axis
- `precision-accuracy-plot.zoom.svg` and `precision-accuracy-plot.zoom.png` are the same figures but scaled on data

Several temporary files (`tmp.*`) can be safely ignored.
Additionaly, a logfile is generated in the `log/` folder next to the `MAIN.sh` script.

## `PAPER-RESULTS/`

This folder essentially contains results from the `RESULT_ANALYSIS/MAIN.sh` script.


# Dependencies

## Software dependencies

In order to run the full benchmark from the read simulation to the binning comparison, the following executables must be in your `$PATH`:

- perl >=5.10.0
- python >=3.2
- Rscript >=3.3
- samtools >=1.7

Tools were run under CentOS 6.7, Ubuntu 16.05.5 LTS and Antergos 18.7.

## R packages

- cowplot
- ggplot2
- gridExtra
- plyr
- vegan

```R
chooseCRANmirror(ind=1)
install.packages("package_name", deps=T)
```
## Perl modules

- Bio::DB::Taxonomy
- Bio::Tree::Tree
- Data::Dumper::Names
- File::Basename
- Getopt::Long
- JSON
- LWP::Protocol::https
- LWP::Simple
- Pod::Usage
- Storable

```bash
cpan -i Module::Name
```
## Python packages

- argparse
- biopython

```bash
pip install package_name
```

Binning tools are run by scripts which manage the dependencies using virtual environments (see [Binning](#binning)).
