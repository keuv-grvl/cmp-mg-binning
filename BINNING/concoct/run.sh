#!/usr/bin/env bash

#--- https://concoct.readthedocs.io/en/latest/complete_example.html


USAGE="usage: $0 <contigs.fa> <reads_1.fa> <reads_2.fa>"
: ${1? $USAGE }
: ${2? $USAGE }
: ${3? $USAGE }

#-- Set up environnement
VENV="concoct-env"

source activate $VENV \
  || conda create -y -n $VENV python=2.7 gcc gsl pip samtools numpy scipy \
  && source activate $VENV

### CONCOCT installation procedure
# mkdir -p $CONDA_PREFIX/share/CONCOCT
# pushd $CONDA_PREFIX/share/
# git clone https://github.com/BinPro/CONCOCT.git
# cd CONCOCT
# git checkout tags/0.4.0
# export CFLAGS="-I$CONDA_PREFIX/include"
# export LDFLAGS="-L$CONDA_PREFIX/lib"
# export CPATH=${CONDA_PREFIX}/include
# ### CHANGE NUMBER OF THREADS TO 32    here-> vv <-here
# grep "N_RTHREADS" c-concoct/vbgmmmodule.h
# sed -i.bak -r 's/(#define N_RTHREADS\s+)10/\132/' c-concoct/vbgmmmodule.h
# grep "N_RTHREADS" c-concoct/vbgmmmodule.h
# python setup.py install --prefix=$CONDA_PREFIX
# cp -r scripts $CONDA_PREFIX/bin
# popd
# pip freeze | grep concoct
CONCOCT_DIR=$CONDA_PREFIX/bin

# download picard 1.118 as in CONCOCT's docker
[ -e "picard-tools-1.118/MarkDuplicates.jar" ] || (
  wget https://downloads.sourceforge.net/project/picard/picard-tools/1.118/picard-tools-1.118.zip  \
  && unzip -q picard-tools-1.118.zip  \
  && rm picard-tools-1.118.zip
) ;
MRKDUP=$(readlink -f picard-tools-1.118/MarkDuplicates.jar)
export MRKDUP


log () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")] $@"
}



set -x


$CONCOCT_DIR/concoct --version # 0.4.0

THREAD=8

CONTIGS=$1 # input/M.Scaffolds.1kb+.split10kb.fasta
READS1=$2 # input/M.reads-reads_1.fasta
READS2=$3 # input/M.reads-reads_2.fasta
EXPERIM=$(basename $CONTIGS .Scaffolds.1kb+.split10kb.fasta)

[ -f $CONTIGS ] || { echo "CONTIGS not found"; exit 1; }
[ -f $READS1 ]  || { echo "READS1 not found";  exit 1; }
[ -f $READS2 ]  || { echo "READS2 not found";  exit 1; }

# fq2fa input/M.reads-reads_1.fastq input/M.reads-reads_1.fasta
# fq2fa input/M.reads-reads_2.fastq input/M.reads-reads_2.fasta




mkdir -p output tmp log
log "START $EXPERIM" > log/${EXPERIM}.time.log

#-- map reads on contigs & mark duplicates & sort
echo "---" >> log/${EXPERIM}.time.log

/usr/bin/time -v -a -o log/${EXPERIM}.time.log            \
  bash $CONCOCT_DIR/scripts/map-bowtie2-markduplicates.sh  \
  -ct 1                                                    \
  -p '-f'                                                  \
  $READS1                                                  \
  $READS2                                                  \
  $EXPERIM                                                 \
  $CONTIGS                                                 \
  $EXPERIM                                                 \
  tmp/ ;

BAM=$(find tmp -name "*.bam")
BAMsort="${BAM%.*}.sorted.${BAM##*.}"
samtools sort -@ $THREAD -m 32G -o "${BAM%.*}.sorted.${BAM##*.}" $BAM



#-- generate coverage table
echo "---" >> log/${EXPERIM}.time.log
/usr/bin/time -v -a -o log/${EXPERIM}.time.log      \
  python $CONCOCT_DIR/scripts/gen_input_table.py     \
  input/$EXPERIM.Scaffolds.1kb+.split10kb.fasta      \
  $BAMsort                                           \
  > tmp/$EXPERIM.concoct_inputtable.tsv ;

cut -f1,3- tmp/$EXPERIM.concoct_inputtable.tsv       \
  > tmp/$EXPERIM.concoct_coverage.tsv ;


#-- run concoct
echo "---" >> log/${EXPERIM}.time.log
/usr/bin/time -v -a -o log/${EXPERIM}.time.log      \
  $CONCOCT_DIR/concoct                           \
  --basename "output/${EXPERIM}"                     \
  --seed 123                                         \
  --coverage_file $(readlink -f tmp/$EXPERIM.concoct_coverage.tsv)  \
  --composition_file $(readlink -f input/$EXPERIM.Scaffolds.1kb+.split10kb.fasta) ;

echo "---" >> log/${EXPERIM}.time.log

log "END $EXPERIM" >> log/${EXPRERIM}.time.log
source deactivate
log "Bye."
