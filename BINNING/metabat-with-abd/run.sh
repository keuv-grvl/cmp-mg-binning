#!/usr/bin/env bash

log () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")]" "$@"
}

set -x

THREADS=8
mkdir -p output log

command -v metabat
command -v jgi_summarize_bam_contig_depths



for F in $(ls input/*.Scaffolds.1kb+.split10kb.fasta); do
  F=$(readlink -f $F)
  FF=$(basename $F)
  EXPERIM=$(basename $F .Scaffolds.1kb+.split10kb.fasta)
  BAM=$(readlink -f $(find input/ -name "${EXPERIM}*.sort.bam"))

  log "Binning '$EXPERIM' using '$BAM'..."
  mkdir -p output/${EXPERIM}
  pushd output/${EXPERIM}
  mkdir -p log

  /usr/bin/time -o "log/$EXPERIM.time.log" -v   \
    runMetaBat.sh                               \
    --sensitive                                 \
    --unbinned                                  \
    --numThreads $THREADS                       \
    --verbose                                   \
    $F $BAM | tee "log/$EXPERIM.log"

  # tar jcvf ${EXPERIM}.metabat.tar.bz2 tmp output log
  popd
done



log "Bye"

# #-- run metabat (TNF only)
# for F in $(ls input/*.fasta); do

#   FF=$(basename $F)
#   EXPERIM=$(basename $F .Scaffolds.1kb+.split10kb.fasta)
#   log "> Binning $EXPERIM..."

#   /usr/bin/time -o "log/$EXPERIM.time.log" -v metabat \
#   --inFile "input/$FF" \
#   --outFile "output/$EXPERIM" \
#   --sensitive \
#   --unbinned \
#   --numThreads $THREADS \
#   --verbose | tee "log/$EXPERIM.log"

# done

# set +x
