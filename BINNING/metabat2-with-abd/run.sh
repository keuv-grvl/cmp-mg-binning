#!/usr/bin/env bash

docker run metabat/metabat metabat2 --help 2>&1 | grep version  # (version 2.12.1; Aug 31 2017 21:02:54)


for F in $(echo S M L); do
  echo "Running metabat2-with-abd on dataset $F"
  mkdir -p "output/${F}/"

  # compute depth from BAM files
  echo "  extract abundances"
  docker run  \
    --volume $(pwd):/data  \
    --user $(id -u):$(id -g)  \
    metabat/metabat  \
    jgi_summarize_bam_contig_depths  \
    --outputDepth /data/output/${F}/${F}.depth.txt  \
    /data/${F}.${F}.Scaffolds.1kb+.split10kb.sort.bam

  # Run MetaBAT2 using composition & abundance
  echo "  binning"
  docker run  \
    --volume $(pwd):/data  \
    --user $(id -u):$(id -g)  \
    metabat/metabat  \
      bash -c "/usr/bin/time -v  \
      metabat2  \
      --inFile /data/${F}.Scaffolds.1kb+.split10kb.fasta  \
      --abdFile /data/output/${F}/${F}.depth.txt  \
      --outFile /data/output/${F}/${F}  \
      -t 8 --unbinned --verbose  \
      2> /data/output/${F}/${F}.metabat2-with-abd.time.log  \
      > /data/output/${F}/${F}.metabat2-with-abd.log"
done
