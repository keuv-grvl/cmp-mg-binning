#!/usr/bin/env bash

docker run metabat/metabat metabat2 --help 2>&1 | grep version  # (version 2.12.1; Aug 31 2017 21:02:54)

for F in $(echo S M L); do
  echo "Running MetaBAT2 on dataset $F"
  mkdir -p "outputbis/${F}/"

  # Tetramer freq only
  docker run  \
    --volume $(pwd):/data  \
    metabat/metabat  \
      bash -c "/usr/bin/time -v  \
      metabat2  \
      --inFile /data/outputbis/${F}/${F}.Scaffolds.1kb+.split10kb.fasta  \
      --outFile /data/outputbis/${F}/${F}  \
      -t 8 --verbose  \
      2> /data/outputbis/${F}/${F}.metabat2.time.log  \
      > /data/outputbis/${F}/${F}.metabat2.log"
done

