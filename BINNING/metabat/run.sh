#!/usr/bin/env bash

log () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")] $@"
}

THREADS=8
mkdir -p tmp output log

command -v metabat

USAGE="usage: $0 <contigs.fa>"
: ${1? $USAGE }

set -x

THREAD=8
INCONTIG=$1


FF=$(basename $INCONTIG)
EXPERIM=$(basename $FF .Scaffolds.1kb+.split10kb.fasta)
log "Binning $EXPERIM..."

/usr/bin/time -o "log/$EXPERIM.time.log" -v  \
  metabat                                    \
  --inFile "input/$FF"                       \
  --outFile "output/$EXPERIM"                \
  --sensitive                                \
  --unbinned                                 \
  --numThreads $THREADS                      \
  --verbose                                  \
  | tee "log/$EXPERIM.log"


set +x
log "bye"

# ./run.sh input/M.Scaffolds.1kb+.split10kb.fasta
