#!/usr/bin/env bash

# ### Install GNU/time utility into the gerners/dastool:1.1.0 container.
# ### This alters the current container.
# $ docker run -it --entrypoint bash gerners/dastool:1.1.0
# # cd /opt/
# # wget https://ftp.gnu.org/gnu/time/time-1.9.tar.gz
# # tar zxvf time-1.9.tar.gz
# # cd time-1.9/
# # ./configure --prefix=/usr/
# # make install
# # which time
# # hostname  # note the CONTAINER_ID
# # exit
# $ docker commit CONTAINER_ID gerners/dastool_time

docker run gerners/dastool_time DAS_Tool --version # 1.1.0

for F in $(echo S M L); do

  FILES=$(find input/ -maxdepth 1 -type f -name "${F}.*" | tr '\n' ',')
  FILES=${FILES: :-1}
  LABELS=$(sed 's/input\///g; s/\.bins\.tsv//g' <<< $FILES)
  CONTIGS="contigs/${F}.Scaffolds.1kb+.split10kb.fasta"
  OUTDIR="output/${F}/"
  mkdir -p "$OUTDIR"
   
  echo "Running DAS_Tool on dataset '$F'"
  echo "  using '$LABELS'"

  docker run  \
    --volume $(pwd):/workdata  \
    --user $(id -u):$(id -g)  \
    --entrypoint /usr/bin/time  \
    gerners/dastool_time  \
    -v -o $OUTDIR/${F}.time.log \
    DAS_Tool \
    -i $FILES  \
    -l $LABELS  \
    -c $CONTIGS  \
    -o $OUTDIR/${F}  \
    --write_bins 1  \
    --search_engine blast  \
    --threads 8

done

echo "Bye."
