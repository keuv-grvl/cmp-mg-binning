#!/usr/bin/env bash

VENV="metaprob-env"

source activate $VENV \
  || conda create -y -n $VENV gcc eigen boost libgcc \
  && source activate $VENV

which MetaProb || { echo "ERROR: MetaProb is not installed."; exit 1;}
# help: https://bitbucket.org/samu661/metaprob


USAGE="usage: $0 <contigs.fa>"
: ${1? $USAGE }

SCAFFOLDS=$1
THREAD=8
EXPERIM=$(basename $SCAFFOLDS .Scaffolds.1kb+.split10kb.fasta)

mkdir -p output tmp log


# work on cut up scaffolds (considered as long reads)
/usr/bin/time -o "log/${EXPERIM}.time.log" -v MetaProb  \
  -si $SCAFFOLDS                                        \
  -dirOutput output/${EXPERIM}.bins/                    \
  -graphType 1                                          \
  -feature 2                                            \
  -m 45                                                 \
  | tee "log/${EXPERIM}.log"



#-- Parameter
# -si               File path single-end reads
# -pi               File paths paired-end reads
# -dirOutput        Path directory output files. Default: output/
# -graphType        0 = pair read in Paired-End read with same ID,
#                   1 = All read Single-End,
#                   2 = All read Single-End and then union groups with Paired-End info.
#                   Default: 0
# -numSp            Number expected specie in file. Default: 2
# -q                Size of q-mer used to create graph adiacences: Default: 30
# -m                Threshold of shared q-mer to create graph adiacences. Default: 5
# -ssize            Max Seed size in each group. Default: 9000
# -lmerFreq         Size of L-mer used to compute feature vector. Default: 4
# -feature          Feature used to compute. Default: 1
# -mg               Only group mode activated (output only the groups created). Default: Not Active
# -eK               Estimate K for K-means algorithm. Default: Active unless you specify -numSp

#-- Advanced Option
# -iterMaxKmeans    Max iteration of Kmeans algorithm. Default: 100
# -timeMaxKmeans    Max time in seconds of Kmeans algorithm. Default: 3600

#-- Feature available
# There are several features available to describe the information contained in the groups, but two of this are the best:
# + For Short Read (bp < 300) -> -feature 1 = NORM_D2star_All_Read_Prob_Lmer_Euclidian
# + For Long Read (bp > 300) -> -feature 2 = NORM_D2star_Group_Prob_Bernulli_Lmer_Euclidian

# -feature 1 = NORM_D2star_All_Read_Prob_Lmer_Euclidian, //NORM_D2star_All_Read_Prob_Kmer + Euclidian norm
# -feature 2 = NORM_D2star_Group_Prob_Bernulli_Lmer_Euclidian, //NORM_D2star_Group_Prob_Bernulli_Kmer + Euclidian norm
# -feature 3 = NORM_BiMeta, //Bimeta's paper norm
# -feature 4 = NORM_D2star_Group_Prob_Lmer, //probability of L-mer in a group, given seed read L-mer's count vector
# -feature 5 = NORM_D2star_All_Read_Prob_Lmer, //probability of L-mer in collection, given seed read L-mer's count vectors
# -feature 6 = NORM_D2star_Group_Prob_Bernulli_Lmer, //probability of L-mer with Bernulli model in a group
# -feature 7 = NORM_D2star_All_Read_Prob_Bernoulli, //probability of L-mer with Bernulli model in collection
# -feature 8 = NORM_D2star_All_Seed_Read_Prob_Bernoulli, //probability of L-mer with Bernulli model in collection seed read
# -feature 9 = NORM_BiMeta_Euclidian, //NORM_Size_Seed + Euclidian norm
# -feature 10 = NORM_D2star_Group_Prob_Lmer_Euclidian, //NORM_D2star_Group_Prob_Kmer + Euclidian norm
# -feature 11 = NORM_D2star_All_Read_Prob_Bernoulli_Euclidian, //NORM_D2star_Prob_Bernoulli_All_Read + Euclidian norm
# -feature 12 = NORM_D2star_All_Seed_Read_Prob_Bernoulli_Euclidian, //NORM_D2star_Prob_Bernoulli_All_Seed_Read + Euclidian norm

#-- Run
# Calls algorithm where is compiled:
# ./MetaProb -si ../TestInputFile/long_example_1.fna -numSp 2 -feature 10 -m 45
# ./MetaProb -pi ../TestInputFile/short_example_1.fna.1 ../TestInputFile/short_example_1.fna.2 -numSp 2
# ./MetaProb -pi ../TestInputFile/short_example_2.fna.1 ../TestInputFile/short_example_2.fna.2 -numSp 2 -feature 9

# Best Parameter
# For Short Read (bp < 300) -> -feature 1 -m 5
# For Long Read (bp > 300) -> -feature 2 -m 45
set +x
source deactivate $VENV
