#!/usr/bin/env bash

set -euo pipefail

# COPY THIS FILE LINE BY LINE IN A TERMINAL

# src: https://github.com/younglululu/COCACOLA

VENV="cocacola-env"

source activate $VENV \
  || conda create -y -n $VENV python=2.7 samtools bowtie2 bedtools pysam biopython pandas \
  && source activate $VENV

which matlab >/dev/null || { echo "matlab not found"; exit 1; }


CONCOCT_dir="$HOME/.opt/CONCOCT/"
[ -d $CONCOCT_dir ] || { echo "CONCOCT_dir not found"; exit 1; }
[ -f $CONCOCT_dir/scripts/fasta_to_features.py ] || { echo "file1 not found"; exit 1; }
[ -f $CONCOCT_dir/scripts/gen_input_table.py ] || { echo "file2 not found"; exit 1; }


## Set up env

: ${1? "usage: $0 <EXPERIM> (S, M, L)" }
EXPERIM=$1

echo "> $EXPERIM";

mkdir -p tmp log output
THREADS=8

SCAFFOLDS="input/${EXPERIM}.Scaffolds.1kb+.split10kb.fasta"
READS1="input/${EXPERIM}.reads-reads_1.fasta"
READS2="input/${EXPERIM}.reads-reads_2.fasta"

[ -e $SCAFFOLDS ] || { echo "File '$SCAFFOLDS' not found"; exit 1; }
[ -e $READS1 ]    || { echo "File '$READS1' not found";    exit 1; }
[ -e $READS2 ]    || { echo "File '$READS2' not found";    exit 1; }

#------------------------------------------------------------------------------#
# à partir de là, l'utilisateur n'intervient plus

set -x

# Preprocessing

INDEX=$SCAFFOLDS

SAM="tmp/${EXPERIM}.reads-on-scaff.sam"
BAM="tmp/${EXPERIM}.reads-on-scaff.bam"
BAMsort="tmp/${EXPERIM}.reads-on-scaff.sort.bam"

[ $RUN == 1 ] || {  set +x; echo "# must run 'RUN=1 $0 $@' to proceed"; exit 42; set -x; }


### index contigs for bowtie2
bowtie2-build   \
  "$SCAFFOLDS"  \
  "$SCAFFOLDS"

### map reads on contigs
bowtie2 -f        \
  --end-to-end    \
  -p $THREADS     \
  -x $INDEX       \
  -1 $READS1      \
  -2 $READS2      \
  -S $SAM


### mark duplicates
samtools view -@ $THREADS -b -S $SAM -o $BAM
samtools sort -@ $THREADS -o $BAMsort $BAM
samtools index $BAMsort $BAMsort.bai


## Coverage table
python $CONCOCT_dir/scripts/gen_input_table.py  \
  $SCAFFOLDS                                    \
  $BAMsort                                      \
  > tmp/${EXPERIM}.cov_inputtableR.tsv

cut -f1,3- tmp/${EXPERIM}.cov_inputtableR.tsv   \
  > tmp/${EXPERIM}.concoct_coverage.tsv

## Composition table
python $CONCOCT_dir/scripts/fasta_to_features.py  \
  $SCAFFOLDS                                      \
  $(grep -c "^>" $SCAFFOLDS)                      \
  4                                               \
  tmp/${EXPERIM}.4mer.tsv

## Linkage table

python $CONCOCT_dir/scripts/bam_to_linkage.py  \
  --max_n_cores $THREADS                       \
  --fullsearch                                 \
  $SCAFFOLDS                                   \
  $BAMsort                                     \
  > tmp/${EXPERIM}.concoct_linkage.tsv

echo "## Preprocessing done"


COCACOLA_HOME="/home/prof/gravouil/.opt/COCACOLA"
export COCACOLA_HOME
export EXPERIM
echo $EXPERIM

/usr/bin/time -o "log/$EXPERIM.time.log" -v \
  matlab -nodisplay -nojvm -nosplash -nodesktop < cocacola.m


set +x
# source deactivate $VENV
echo "bye"
