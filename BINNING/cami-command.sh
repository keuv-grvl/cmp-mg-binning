#!/usr/bin/env bash


# Commands for CAMI results integration into benchmark

# CAMI.scaffold-ids.txt
grep -v -e "^#" -e "^$" -e "^@" gsa_mapping_pool_with_lenghts.binning  \
  | cut -f1 > CAMI.scaffold-ids.txt


# Scaffold -> taxonomic id
grep -v -e "^#" -e "^$" -e "^@" gsa_mapping_pool_with_lenghts.binning  \
  | cut -f1,3 > CAMI.scaffold-taxid.tsv



echo "$0 is not intended to be run"
exit 1

python ./amber.py  \
-g CAMI_raw_results/gsa_mapping_pool_with_lenghts.binning  \
-r CAMI_raw_results/unique_common.tsv  \
-k "circular element"  \
-p 1  \
CAMI_raw_results/goofy_hypatia_0  \
CAMI_raw_results/naughty_carson_0  \
CAMI_raw_results/maxbin224.bins  \
CAMI_raw_results/admiring_curie_0  \
CAMI_raw_results/metabat2.11.2.bins  \
CAMI_raw_results/berserk_hypatia_0  \
CAMI_raw_results/berserk_euclid_0  \
CAMI_raw_results/binsanity.bins  \
CAMI_raw_results/binsanity-wf.bins  \
CAMI_raw_results/result_highy.bins  \
CAMI_raw_results/dastool.das_DASTool_scaffolds2bin.txt.bin  \
-l "CONCOCT (CAMI),MaxBin 2.0.2 (CAMI), MaxBin 2.2.4, MetaBAT (CAMI), MetaBAT 2.11.2, Metawatt 3.5 (CAMI), MyCC (CAMI), Binsanity 0.2.5.9, Binsanity-wf 0.2.5.9, COCACOLA, DAS Tool 1.1"  \
-o output_dir


