#!/usr/bin/env bash

VENV="maxbin-env"

source activate $VENV \
  || conda create -y -n $VENV python=2.7 bowtie2 hmmer idba fastx_toolkit r-base fraggenescan \
  && source activate $VENV

# http://downloads.sourceforge.net/project/maxbin/MaxBin-2.2.1.tar.gz
which run_MaxBin.pl || { echo "ERROR: MaxBin is not installed."; exit 1;}


USAGE="usage: $0 <contigs.fa> <reads.pe.fa>"
: ${1? $USAGE }
: ${2? $USAGE }

set -x

# fq2fa --merge S.reads-reads_1.fastq S.reads-reads_2.fastq S.PE-reads.fasta

THREAD=8
INCONTIG=$1
INREAD=$2
EXPERIM=$(basename $INCONTIG .Scaffolds.1kb+.split10kb.fasta)

mkdir -p output tmp log


/usr/bin/time -o "log/$EXPERIM.time.log" -v run_MaxBin.pl  \
  -contig $INCONTIG                                        \
  -reads $INREAD                                           \
  -out output/$EXPERIM                                     \
  -thread $THREAD                                          \
  -plotmarker                                              \
  -verbose                                                 \
  -preserve_intermediate \
  | tee "log/${EXPERIM}.log"


# MaxBin 2.2.1
# MaxBin - a metagenomics binning software.
# Usage:
#   run_MaxBin.pl
#     -contig (contig file)
#     -out (output file)

#    (Input reads and abundance information)
#     [-reads (reads file) -reads2 (readsfile) -reads3 (readsfile) -reads4 ... ]
#     [-abund (abundance file) -abund2 (abundfile) -abund3 (abundfile) -abund4 ... ]

#    (You can also input lists consisting of reads and abundance files)
#     [-reads_list (list of reads files)]
#     [-abund_list (list of abundance files)]

#    (Other parameters)
#     [-min_contig_length (minimum contig length. Default 1000)]
#     [-max_iteration (maximum Expectation-Maximization algorithm iteration number. Default 50)]
#     [-thread (thread num; default 1)]
#     [-prob_threshold (probability threshold for EM final classification. Default 0.9)]
#     [-plotmarker]
#     [-reassembly]
#     [-markerset (marker gene sets, 107 (default) or 40.  See README for more information.)]

#   (for debug purpose)
#     [-version] [-v] (print version number)
#     [-verbose]
#     [-preserve_intermediate]

#   Please specify either -reads or -abund information.
#   You can input multiple reads and/or abundance files at the same time.
#   Please read README file for more details.
set +x
source deactivate $VENV
echo "bye"
