#!/usr/bin/env bash
set -euo pipefail
VENV="bwa-env"

source activate $VENV \
  || conda create -y -n $VENV bwa=0.7.15 \
  && source activate $VENV

# set -x
# bwa 2>&1| grep Version # 0.7.15-r1140


log () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")] $@"
}

#--- Set up env ---
USAGE="usage: $0 <contigs.fa> <reference.fa>"
: ${1? $USAGE }
: ${2? $USAGE }

set -x

THREAD=32

CONTIGS=$1   # input/M.Scaffolds.1kb+.split10kb.fasta
REFERENCE=$2 # input/M.reads-reads_1.fasta

[ -f $CONTIGS ]   || { echo "CONTIGS not found";   exit 1; }
[ -f $REFERENCE ] || { echo "REFERENCE not found"; exit 1; }

EXPERIM=$(basename $REFERENCE .ALL.GENOMES.fasta)
REFIDX="$(basename $REFERENCE).idx"


OUTSAM="output/$(basename $REFERENCE .fasta).sam"
OUTBAM="output/$(basename $REFERENCE .fasta).bam"

mkdir -p output tmp log


#--- indexing ---
log "# indexing scaffolds"

/usr/bin/time -o "log/${EXPERIM}.index.time.log" -v  \
  bwa index $REFERENCE -p $REFIDX 2>&1
log "index done"


#--- mapping ---
log "# running mapping"

# Usage: bwa mem [options] <idxbase> <in1.fq> [<in2.fq>]
/usr/bin/time -o "log/${EXPERIM}.mapping.time.log" -v  \
  bwa mem \
  -t $THREAD \
  -x intractg \
  $REFIDX \
  $CONTIGS \
  > $OUTSAM
log "mapping done"


#--- sam to bam ---
log "# converting SAM to BAM"

/usr/bin/time -o "log/${EXPERIM}.sam2bam.time.log" -v  \
  samtools view -@ $THREAD -S -b $OUTSAM > $OUTBAM
rm $OUTSAM
md5sum $OUTBAM > $OUTBAM.md5
log "conversion done"

set +x
# source deactivate $VENV
echo "bye"


#  ./run.sh input/M.Scaffolds.1kb+.split10kb.fasta input/M.ALL.GENOMES.fasta
