#!/bin/bash

#SBATCH --job-name=ray.L.assembly
#SBATCH --nodes=1
#SBATCH --ntasks=32
#SBATCH --mem=120G
#SBATCH --partition=normal

#SBATCH --error="/home/kegravou/COMPARATIF-BINNING/log/ray.L.%J.err"
#SBATCH --output="/home/kegravou/COMPARATIF-BINNING/log/ray.L.%J.out"

#SBATCH --mail-user=kevin.gravouil@udamail.fr
#SBATCH --mail-type=ALL

# usage: sbatch $HOME/COMPARATIF-BINNING/scripts/assembly-ray.sh



echo "Hello! [$(date +"%Y-%m-%d %T")]"
echo "JOB ID=$SLURM_JOB_ID"

#while true; do echo -e "$(date +"%Y-%m-%d %T")\t$(memu)" >> $HOME/log/memusage.$(hostname).ray.log; sleep 150; done &

module load openmpi ray
mpirun Ray -version


WDIR="/storage/scratch/kegravou/$SLURM_JOB_ID"
mkdir -p $WDIR
OUTDIR="$WDIR/ray.L/"


echo "# copying files [$(date +"%Y-%m-%d %T")]"
cp $HOME/COMPARATIF-BINNING/data/L.*.gz $WDIR
ls -la $WDIR
echo "copy done"


echo "# running assembly [$(date +"%Y-%m-%d %T")]"
mpirun -n 32 Ray \
	-show-memory-usage \
	-p $WDIR/L.reads-reads_1.fq.gz $WDIR/L.reads-reads_2.fq.gz \
	-o $OUTDIR
echo "assembly done"


echo "# collecting results [$(date +"%Y-%m-%d %T")]"
cp -r $OUTDIR $HOME/COMPARATIF-BINNING/results/
echo "collecting done"

echo "# cleaning [$(date +"%Y-%m-%d %T")]"
rm -Rf $WDIR
echo "cleaning done"

echo "Bye. [$(date +"%Y-%m-%d %T")]"



