mpiexec -n 32 Ray \
 -show-memory-usage \
 -p \
 /storage/scratch/kegravou/3011762/M.reads-reads_1.fastq.gz \
 /storage/scratch/kegravou/3011762/M.reads-reads_2.fastq.gz \
 -o \
 /storage/scratch/kegravou/3011762/ray.M/
