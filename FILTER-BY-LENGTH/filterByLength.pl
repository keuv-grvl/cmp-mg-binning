#!/usr/bin/env perl

# adapted from: https://www.biostars.org/p/79202/

use strict;
use warnings;

die "usage: perl $0 <minimum length> <fasta file>\n" unless (scalar(@ARGV) >= 2);
my $minlen = shift;

{
    local $/=">";
    while(<>) {
        chomp;
        next unless /\w/;
        s/>$//gs;
        my @chunk = split /\n/;
        my $header = shift @chunk;
        my $seqlen = length join "", @chunk;
        print ">$_" if($seqlen >= $minlen);
    }
    local $/="\n";
}

__END__
