#!/usr/bin/env bash

# Download the latest NCBI taxonomy database

#SOURCE: https://techoverflow.net/blog/2013/12/30/accessing-ncbi-ftp-via-rsync/

set -euo pipefail

echo "> Will update NCBI taxonomy and accession2taxid databases"

DB_DIR="ncbi_taxdb"
DEST=$(date +%Y%m%d)

cd $DB_DIR
mkdir -p "${DEST}"

echo "> new folder: ${DB_DIR}/${DEST}"

pushd "${DEST}"

curl -OL ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
curl -OL ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz.md5
md5sum -c taxdump.tar.gz.md5  \
  && tar zxvf taxdump.tar.gz  \
  && rm taxdump.tar.gz taxdump.tar.gz.md5

curl -OL ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz.md5
curl -OL ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz
md5sum -c nucl_gb.accession2taxid.gz.md5  \
  && tar zxvf nucl_gb.accession2taxid.gz  \
  && rm nucl_gb.accession2taxid.gz nucl_gb.accession2taxid.gz.md5

popd;

ln -sfn $DEST latest

echo "> done"
