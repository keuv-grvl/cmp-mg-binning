#!/usr/bin/env bash

# Download reference genomes to evaluate binning software


### User-defined variables ###

OUTFILE="S.strains.csv"

#-- Number of reference genomes to get (uncommented what is needed)
#S
NB_BACT=58
NB_FUNG=1
NB_PROT=1
NB_ARCH=1
NB_VIRA=3
# #total=64

#M
# NB_BACT=154
# NB_FUNG=2
# NB_PROT=1
# NB_ARCH=2
# NB_VIRA=12
# #total:171

#L
# NB_BACT=318
# NB_FUNG=3
# NB_PROT=1
# NB_ARCH=2
# NB_VIRA=26
# #total=350

### End of user-defined variables ###




function cleanup {
	echo -n "> cleaning... "
	mkdir -p genomes.S genomes.M genomes.L
	rm tmp.*
	rm *_assembly_summary.txt
	rm genomes.S/* genomes.M/* genomes.L/*
	echo "ok"
}

# cleanup


echo "IS NCBI/taxo UP-TO-DATE? (ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/)"
GETLINEAGE=./GetCompleteLineage.pl



echo -e "#ACCESSION\tTAXID\tORGANISM_NAME\tSTRAIN\tFTP_PATH\tTAXID2\tKINGDOM\tPHYLUM\tCLASS\tORDER\tFAMILY\tGENUS\tSPECIES" > $OUTFILE

#--- download list of ref genomes
KINGDOMS=(protozoa fungi viral bacteria archaea)
echo "> downloading lists"

for K in "${KINGDOMS[@]}"; do
	echo -n " - $K... "
	curl -s "ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/${K}/assembly_summary.txt" > "${K}_assembly_summary.txt"
 	echo "ok"
done


BACT="bacteria_assembly_summary.txt"
FUNG="fungi_assembly_summary.txt"
PROT="protozoa_assembly_summary.txt"
VIRA="viral_assembly_summary.txt"
ARCH="archaea_assembly_summary.txt"


echo "> selecting $(( $NB_BACT + $NB_FUNG + $NB_PROT + $NB_ARCH + $NB_VIRA )) strains... "


#--- all
echo -e "#ACCESSION\tTAXID\tORGANISM_NAME\tSTRAIN\tFTP_PATH\tTAXID2\tKINGDOM\tPHYLUM\tCLASS\tORDER\tFAMILY\tGENUS\tSPECIES" > all.strains.csv

echo -n " - select all... "
grep --no-filename -v "^#" $FUNG $ARCH $BACT $VIRA $PROT                                            \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome") print $0;}'    \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 > tmp.all.strains
echo "ok"


echo -n "  * getting lineage... "
$GETLINEAGE $(cut -f2 tmp.all.strains | tr "\n" " ") | tr ";" "\t" > tmp.all.lineage
echo "ok"

echo -n "  * compiling data... "
paste tmp.all.strains tmp.all.lineage >> all.strains.csv
echo "ok"



#--- select protozoa
echo -n " - $NB_PROT protozoa... "
grep -v "^#" $PROT                                                                    \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome" ) print $0;}'   \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 > tmp.P.strains
echo "ok"

echo -n "  * getting lineage... "
$GETLINEAGE $(cut -f2 tmp.P.strains | tr "\n" " ") | tr ";" "\t" > tmp.P.lineage
#cut -f2 tmp.P.strains | parallel -k "$GETLINEAGE {}" | tr ";" "\t" > tmp.P.lineage
echo "ok"

echo -n "  * compiling data... "
paste tmp.O.strains tmp.P.lineage | sort -k8,8 -k9,9 -k10,10 -k11,11 -k12,12 -k13,13 >> $OUTFILE
echo "ok"

#--- select fungi
echo -n " - $NB_FUNG fungi... "
grep -v "^#" $FUNG                                                                    \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome") print $0;}'    \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 | shuf -n $NB_FUNG > tmp.F.strains
echo "ok"

echo -n "  * getting lineage... "
$GETLINEAGE $(cut -f2 tmp.F.strains | tr "\n" " ") | tr ";" "\t" > tmp.F.lineage
# cut -f2 tmp.F.strains | parallel -k "$GETLINEAGE {}" | tr ";" "\t" > tmp.F.lineage
echo "ok"

echo -n "  * compiling data... "
paste tmp.F.strains tmp.F.lineage >> $OUTFILE
echo "ok"

#--- select viral
echo -n " - $NB_VIRA viruses... "
grep -v "^#" $VIRA                                                                    \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome") print $0;}'    \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 | shuf -n $NB_VIRA > tmp.V.strains
echo "ok"

echo -n "  * getting lineage... "
$GETLINEAGE $(cut -f2 tmp.V.strains | tr "\n" " ") | tr ";" "\t" > tmp.V.lineage
# cut -f2 tmp.V.strains | parallel -k "$GETLINEAGE {}" | tr ";" "\t" > tmp.V.lineage
echo "ok"

echo -n "  * compiling data... "
paste tmp.V.strains tmp.V.lineage >> $OUTFILE
echo "ok"

#--- select bacteria
echo -n " - $NB_BACT bacteria... "
grep -v "^#" $BACT                                                                    \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome") print $0;}'    \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 | shuf -n $NB_BACT > tmp.B.strains
echo "ok"

echo -n "  * getting lineage... "
# cut -f2 tmp.B.strains | parallel -k "$GETLINEAGE {}" | tr ";" "\t" > tmp.B.lineage
$GETLINEAGE $(cut -f2 tmp.B.strains | tr "\n" " ") | tr ";" "\t" > tmp.B.lineage
echo "ok"

echo -n "  * compiling data... "
paste tmp.B.strains tmp.B.lineage >> $OUTFILE
echo "ok"

#--- select archaea
echo -n " - $NB_ARCH archaea... "
grep -v "^#" $ARCH                                                                    \
 | awk -F $'\t' '{if ($5 != "na") print $0;}'                                         \
 | awk -F $'\t' '{if ($11 == "latest") print $0;}'                                    \
 | awk -F $'\t' '{if ($12 == "Complete Genome" || $12 == "Chromosome") print $0;}'    \
 | awk -F $'\t' '{if ($14 == "Full") print $0;}'                                      \
 | awk -F $'\t' '{print $1"\t"$6"\t"$8"\t"$9"\t"$20}'                                 \
 | shuf -n $NB_ARCH > tmp.A.strains
echo "ok"

echo -n "  * getting lineage... "
# cut -f2 tmp.A.strains | parallel -k "$GETLINEAGE {}" | tr ";" "\t" > tmp.A.lineage
$GETLINEAGE $(cut -f2 tmp.A.strains | tr "\n" " ") | tr ";" "\t" > tmp.A.lineage

echo "ok"

echo -n "  * compiling data... "
paste tmp.A.strains tmp.A.lineage >> $OUTFILE
echo "ok"




#--- DOWNLOAD INDIVIDUAL GENOMES --------------

#--- S
echo "> downloading individual genomes (S)... "
for I in $(grep -v "^#" S.64.strains.csv | cut -f5); do
	I0=${I%/*}         # ftp://ftp.ncbi.nlm.nih.gov/genomes/all
	II=$(basename $I)  # GCF_001314975.1_ASM131497v1
	I1=${II:0:3}       # GCF
	I2=${II:4:3}       # 001
	I3=${II:7:3}       # 314
	I4=${II:10:3}      # 975
	URL="$I0/$I1/$I2/$I3/$I4/$II/${II}_genomic.fna.gz"
	echo " - $II"
	pushd genomes.S/ > /dev/null
	curl -sOL $URL
	popd > /dev/null
done

gunzip genomes.S/*.fna.gz

echo -n "> merging genomes... "
rm genomes.S/S.ALL.GENOMES.fasta
for F in $(ls genomes.S/*.fna | shuf); do
	cat $F >> genomes.S/S.ALL.GENOMES.fasta
done
echo "ok"


#--- M
echo "> downloading individual genomes (M)... "
for I in $(grep -v "^#" M.171.strains.csv | cut -f5); do
	I0=${I%/*}         # ftp://ftp.ncbi.nlm.nih.gov/genomes/all
	II=$(basename $I)  # GCF_001314975.1_ASM131497v1
	I1=${II:0:3}       # GCF
	I2=${II:4:3}       # 001
	I3=${II:7:3}       # 314
	I4=${II:10:3}      # 975
	URL="$I0/$I1/$I2/$I3/$I4/$II/${II}_genomic.fna.gz"
	echo " - $II"
	pushd genomes.M/ > /dev/null
	curl -sOL $URL
	popd > /dev/null
done

gunzip genomes.M/*.fna.gz

echo -n "> merging genomes... "
cat genomes.M/*.fna > genomes.M/M.ALL.GENOMES.fasta
echo "ok"


#--- L
echo "> downloading individual genomes (L)... "
for I in $(grep -v "^#" L.350.strains.csv | cut -f5); do
	I0=${I%/*}         # ftp://ftp.ncbi.nlm.nih.gov/genomes/all
	II=$(basename $I)  # GCF_001314975.1_ASM131497v1
	I1=${II:0:3}       # GCF
	I2=${II:4:3}       # 001
	I3=${II:7:3}       # 314
	I4=${II:10:3}      # 975
	URL="$I0/$I1/$I2/$I3/$I4/$II/${II}_genomic.fna.gz"
	echo " - $II"
	pushd genomes.L/ > /dev/null
	curl -sOL $URL
	popd > /dev/null
done

gunzip genomes.L/*.fna.gz

echo -n "> merging genomes... "
cat genomes.L/*.fna > genomes.L/L.ALL.GENOMES.fasta
echo "ok"
