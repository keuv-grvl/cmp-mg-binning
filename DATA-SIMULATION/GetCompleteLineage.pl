#!/usr/bin/perl

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
#- BioPerl and stuff
use Bio::DB::Taxonomy;
use Bio::Tree::Tree;

GetOptions (
    "help" => sub { pod2usage(-verbose => 1); exit },
    "h" => sub { pod2usage(-verbose => 0); exit },
    "man" => sub { pod2usage(-verbose => 2); exit }
) or pod2usage(-verbose => 1);

my($TAX_DIR, $NODE_FILE, $NAMES_FILE, @RANKS);
$TAX_DIR="./ncbi_taxdb/latest/";
$NODE_FILE="$TAX_DIR/nodes.dmp";
$NAMES_FILE="$TAX_DIR/names.dmp";
@RANKS = ("superkingdom","phylum","class","order","family","genus","species");

pod2usage(
    -msg => "[ERROR] Missing argument\n",
    -verbose => 1
) if (scalar(@ARGV) < 1);

my (@INPUT_IDS) = @ARGV;
my $taxdb = Bio::DB::Taxonomy->new(
    -source => 'flatfile',
    -directory => $TAX_DIR,
    -nodesfile => $NODE_FILE,
    -namesfile => $NAMES_FILE
) or die "Cannot load taxonomy";

foreach my $INPUT_ID (@INPUT_IDS) {
    my $node = $taxdb->get_taxon(-taxonid => $INPUT_ID);
    unless (defined($node)) {
        print $INPUT_ID,"\t-\n";
        next;
    }

    my $tree = Bio::Tree::Tree->new(-node => $node) or next;

    my @lineage;
    foreach (@RANKS) {
        my $phylumnode = $tree->find_node(-rank => $_);
        if($phylumnode) {
            push @lineage, $phylumnode->scientific_name;
        } else {
            push @lineage, "";
        }
    }

    print $INPUT_ID,"\t";
    print join ";", @lineage;
    print "\n";
}

__END__

=pod

=head1 SYNOPSIS

       perl GetCompleteLineage.pl --help
       perl GetCompleteLineage.pl <TAXID_1> ... <TAXID_N>

=head1 DESCRIPTION

       Take a NCBI taxonomic identifier then return the complete
       lineage (';'-separated).

=head1 ARGUMENTS

       Taxonomic identifier are NCBI taxonomic ID.

       Taxonomic ranks are: "superkingdom", "phylum", "class",
       "order", "family", "genus" and "species".

=head1 REQUIREMENTS

       The NCBI taxonomy must be installed on your system. Recquired
       files are 'nodes.dmp' and 'names.dmp'. You can change the
       taxonomy directory by setting the '$TAX_DIR' variable.

=head1 AUTHOR

       Kevin Gravouil <k.gravouil@gmail.com>

=head1 DATA SOURCES

       ftp://ftp.ncbi.nih.gov/pub/taxonomy/
       ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz (34MB)

=cut

