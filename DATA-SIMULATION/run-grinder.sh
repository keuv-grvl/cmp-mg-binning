#!/usr/bin/env bash

#-------------------------------------------------------------------------------
# generate reads from reference genomes
#-------------------------------------------------------------------------------


# eventually run in a screen as it may take a very long time
# screen -dmS gravouil.grinder
# screen -list
# screen -r gravouil.grinder

grinder -reference_file "genomes.S/S.ALL.GENOMES.fasta" \
 -random_seed 123                         \
 -total_reads 108000000                   \
 -insert_dist 222 normal 5                \
 -mate_orientation FR                     \
 -read_dist 101                           \
 -mutation_dist poly4 3e-3 3.3e-8         \
 -exclude_chars "NX"                      \
 -qual_levels 30 10                       \
 -fastq_output 1                          \
 -base_name "S.reads"                     \
 -output_dir "genomes.S"                  \
 -abundance_file "S.abundances.csv"


grinder -reference_file "genomes.M/M.ALL.GENOMES.fasta" \
 -random_seed 123                         \
 -total_reads 108000000                   \
 -insert_dist 222 normal 5                \
 -mate_orientation FR                     \
 -read_dist 101                           \
 -mutation_dist poly4 3e-3 3.3e-8         \
 -exclude_chars "NX"                      \
 -qual_levels 30 10                       \
 -fastq_output 1                          \
 -base_name "M.reads"                     \
 -output_dir "genomes.M"                  \
 -abundance_file "M.abundances.csv"


grinder -reference_file "genomes.L/L.ALL.GENOMES.fasta" \
 -random_seed 123                         \
 -total_reads 108000000                   \
 -insert_dist 222 normal 5                \
 -read_dist 101                           \
 -mutation_dist poly4 3e-3 3.3e-8         \
 -exclude_chars "NX"                      \
 -qual_levels 30 10                       \
 -fastq_output 1                          \
 -base_name "L.reads"                     \
 -output_dir "genomes.L"                  \
 -abundance_file "L.abundances.csv"


#-- split paired end fastq into _1.fq and _2.fq files
pv "genomes.S/S.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/1"  \
  > "genomes.S/S.reads-reads_1.fastq"
pv "genomes.S/S.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/2"  \
  > "genomes.S/S.reads-reads_2.fastq"

pv "genomes.M/M.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/1"  \
  > "genomes.M/M.reads-reads_1.fastq"
pv "genomes.M/M.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/2"  \
  > "genomes.M/M.reads-reads_2.fastq"

pv "genomes.L/L.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/1"  \
  > "genomes.L/L.reads-reads_1.fastq"
pv "genomes.L/L.reads-reads.fastq"  \
  | grep -A3 -P --no-group-separator "^@\d+/2"  \
  > "genomes.L/L.reads-reads_2.fastq"
