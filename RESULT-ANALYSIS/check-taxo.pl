#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use 5.10.0;
use File::Basename;
use JSON qw(decode_json encode_json);
use Storable;
use LWP::Simple;
use LWP::Protocol::https;    # ensure LWP will handle HTTPS
use Bio::DB::Taxonomy;
use Bio::Tree::Tree;
use Data::Dumper::Names;
$Data::Dumper::Indent = 1;

my $USAGE = "usage: $0 <EXPERIM LABEL>\n";

# general info
my $DATADIR       = "data";
my $ASSOCDIR      = "assoc";
my $EXPERIM       = shift @ARGV or die $USAGE;
my $SCAFFOLD_LIST = $EXPERIM . ".scaffold-ids.txt";
my $HEADER        = "#name\tbin";

# taxdb info
my $TAX_DIR    = "ncbi_taxdb/latest/";
my $NODE_FILE  = "$TAX_DIR/nodes.dmp";
my $NAMES_FILE = "$TAX_DIR/names.dmp";
my @RANKS      = (
    "superkingdom", "phylum", "class",   "order",
    "family",       "genus",  "species", "subspecies"
);

my $taxdb = Bio::DB::Taxonomy->new(
    -source    => 'flatfile',
    -directory => $TAX_DIR,
    -nodesfile => $NODE_FILE,
    -namesfile => $NAMES_FILE
) or die "Cannot load taxonomy";

my $h = {};    # global hash which will store binning and taxonomic data

sub logger {
    my ( $sec, $min, $hour, $mday, $mon, $year ) = localtime();
    printf(
        "[%04d-%02d-%02d %02d:%02d:%02d] %s\n",
        $year + 1900,
        $mon + 1, $mday, $hour, $min, $sec, join( " - ", @_ )
    );
}

sub progressbar {
    my ( $cur, $max ) = @_;
    my $len   = 50;
    my $nbcol = qx#tput cols#;
    chomp($nbcol);

    print "\r[";
    print "=" x ( $cur / $max * $len );
    print "-" x ( $len - ( $cur / $max * $len ) );
    print "] ";
    printf "~%6.2f ", $cur / $max * 100;
    print "%";
    print " " x ( $nbcol - $len - 12 );
}

#-- read data ------------------------------------------------------------------

#-- scaffold list
logger "# loading scaffolds";

open( IN_SCAFF, "<", $SCAFFOLD_LIST );
while (<IN_SCAFF>) {
    next if m/^#/;
    chomp;
    my ( $scaffold, $cluster ) = split /\t/;
    $h->{$scaffold} = {};

    # $h->{$scaffold}->{'bins'} = "UNBINNED";

}
close(IN_SCAFF);

logger( "Number of scaffolds", scalar( keys( %{$h} ) ) );

#-- load scaffold/accessionid link
logger "# loading associated chromosomes";

open( IN_DATA, "<", "$DATADIR/$EXPERIM.reference.tsv" );
while (<IN_DATA>) {
    next if m/^#/;
    chomp;
    my ( $scaffold, $accessionid ) = split /\t/;
    $h->{$scaffold}->{'accessionid'} = $accessionid;
}
close(IN_DATA);

#-- load binning results
logger "# loading 'data' dir";

chomp( my @binfiles = qx#ls $DATADIR/$EXPERIM.*.tsv# );

for my $f (@binfiles) {
    my $ff = basename($f);
    $ff =~ s/\.tsv$//;
    $ff =~ s/\.bins$//;
    $ff =~ s/^$EXPERIM\.//;
    logger "  - $ff ($f)";
    open( IN, "<", $f );
    while (<IN>) {
        next if m/^#/;
        chomp;
        my ( $scaffold, $cluster ) = split /\t/;
        $h->{$scaffold}->{$ff}->{'bins'} = $cluster;
    }
    close(IN);
}

### I have in '$h' the actual results of the different binning software.

#-- load assoc results
logger "# loading 'assoc' dir";

chomp( my @assocfiles = qx#ls $ASSOCDIR/$EXPERIM.*.csv# );

# keep track of the accession ids (without duplicates)
my $accid_file = "tmp." . $EXPERIM . ".accverid.lst";
my $accid_uniq = {};

open( ACCOUT, ">", $accid_file );

for my $f (@assocfiles) {
    my $ff = basename($f);
    $ff =~ s/\.csv$//;
    $ff =~ s/\.bins$//;
    $ff =~ s/^$EXPERIM\.//;
    logger "  - $ff ($f)";

    open( IN, "<", $f );
    while (<IN>) {
        next if m/^#/;
        chomp;
        my ( $accessionid, $cluster ) = split /;/;

        if ( !exists( $accid_uniq->{$accessionid} ) ) {
            $accid_uniq->{$accessionid} = undef;
            say ACCOUT $accessionid;
        }

        foreach my $k ( keys %{$h} ) {
            my $acc = $h->{$k}->{'accessionid'};
            if ( $accessionid eq $acc ) {
                $h->{$k}->{$ff}->{'assoc'} = $cluster;
            }
        }
    }
    close(IN);
}
close(ACCOUT);

# ensure that REFs are associated with themselves
foreach my $k ( keys %{$h} ) {
    warn "error with ref of '$k'"
        if ( $h->{$k}->{'reference'}->{'bins'} ne
        $h->{$k}->{'reference'}->{'assoc'} );
}

# print Dumper $h;
# {
#     local ($|) = (1);
#     print "Checkpoint 1, press Enter to continue";
#     my $resp = <STDIN>;
# }

#-- get taxids of each sequence -------------------------------------------------

logger "# Collecting taxonomic identifiers";

my $cami_scaf2taxid = {};
my $accid2taxid     = {};

# special case: CAMI provides taxids, just have to keep track of it
if ( $EXPERIM =~ m/^CAMI/ and -f $EXPERIM . ".scaffold-taxid.tsv" ) {
    logger "CAMI data, do not need to lookup for taxid from nuccoreid";

    open( IN, "<", $EXPERIM . ".scaffold-taxid.tsv" );
    while (<IN>) {
        next if m/^#/;
        chomp;
        my ( $seqid, $taxid ) = split /\t/;
        $cami_scaf2taxid->{$seqid} = $taxid;
    }
    close(IN);
}

# otherwise, load the accid-taxid association from nucl_gb.accession2taxid
# (ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/)
else {
    my $dbfile = $TAX_DIR . "nucl_gb.accession2taxid";
    my $res    = qx#grep -F --file $accid_file $dbfile#;
    chomp($res);

    foreach ( split /\n/, $res ) {
        my ( undef, $accverid, $taxid, undef ) = split /\t/;
        $accid2taxid->{$accverid} = $taxid;
    }
}

foreach my $k ( keys %{$h} ) {
    my $taxid = "131567";    # default to 'cellular organisms'

    if ( exists $cami_scaf2taxid->{$k} ) {
        $taxid = $cami_scaf2taxid->{$k};
    }
    elsif ( exists $accid2taxid->{ $h->{$k}->{'accessionid'} } ) {
        $taxid = $accid2taxid->{ $h->{$k}->{'accessionid'} };
    }

    $h->{$k}->{'taxonomy'}->{'taxid'} = $taxid;
}

# get lineage for each sequence

logger "# Collecting taxonomic informations (name and lineage)";
my $i = 0;
foreach my $k ( keys %{$h} ) {
    my $taxid = $h->{$k}->{'taxonomy'}->{'taxid'};

    my $node = $taxdb->get_taxon( -taxonid => $taxid );
    unless ( defined($node) ) {
        $h->{$k}->{'taxonomy'}->{'name'} = "UNKNOWN";
        @{ $h->{$k}->{'taxonomy'}->{'lineage'} }
            = ( "-", "-", "-", "-", "-", "-", "-", "-" );
        next;
    }
    my $taxname = $node->scientific_name;

    my $tree = Bio::Tree::Tree->new( -node => $node );
    unless ( defined($tree) ) {
        $h->{$k}->{'taxonomy'}->{'name'} = $taxname;
        @{ $h->{$k}->{'taxonomy'}->{'lineage'} }
            = ( "-", "-", "-", "-", "-", "-", "-", "-" );
        next;
    }

    my (@lineage);
    my $prev = "ERROR";
    foreach (@RANKS) {
        my $n
            = ( $tree->find_node( -rank => $_ ) )
            ? $tree->find_node( -rank => $_ )->scientific_name
            : $prev;
        $prev = $n;    # display name of previous rank if none can be found
        push @lineage, $n;
    }

    $h->{$k}->{'taxonomy'}->{'name'} = $taxname;
    @{ $h->{$k}->{'taxonomy'}->{'lineage'} } = @lineage;

    progressbar( $i, scalar( keys( %{$h} ) ) )
        if ( ++$i % ( scalar( keys( %{$h} ) ) / 150 ) == 0 );
}

#--- print final results

# Get tool list
my @NOTTOOLS = qw/taxonomy accessionid reference/;
my @TOOLS;

foreach my $k ( keys %{$h} ) {

    # get the tool names from the columns name
    foreach my $c ( keys %{ $h->{$k} } ) {
        next if $c ~~ @NOTTOOLS;
        # logger "> " . $c;
        push( @TOOLS, $c );
    }
    last;
}

@TOOLS = sort @TOOLS;
unshift( @TOOLS, 'reference' );    # add the reference in position
# print Dumper @TOOLS;

# print results
logger "Print results to 'diffs/$EXPERIM.diff.bins.assoc.corr.tsv'";
open( OUT, '>', "diffs/$EXPERIM.diff.bins.assoc.corr.tsv" );

say OUT join( "\t",
    "#scaffold-id", "accession-id", map       {uc} @TOOLS, "TAXID",
    "TAXNAME",      "SUPERKINGDOM", "PHYLUM", "CLASS",
    "ORDER",        "FAMILY",       "GENUS",  "SPECIES",
    "SUBSPECIES" );

foreach my $k ( keys %{$h} ) {
    print OUT $k;
    print OUT "\t", $h->{$k}->{'accessionid'};

    # display the correct bins (1 or 0) for each tool
    foreach my $t (@TOOLS) {
        no warnings;
        my $bin = $h->{$k}->{$t}->{'bins'};
        $bin =~ s/^0+//g;    # remove leading '0'
        my $assoc = $h->{$k}->{$t}->{'assoc'};
        $assoc =~ s/^0+//g;    # remove leading '0'
        print OUT "\t", ( $bin eq $assoc ) ? 1 : 0;
    }

    print OUT "\t", $h->{$k}->{'taxonomy'}->{'taxid'};
    print OUT "\t", $h->{$k}->{'taxonomy'}->{'name'};
    print OUT "\t", join( "\t", @{ $h->{$k}->{'taxonomy'}->{'lineage'} } );
    print OUT "\n";

}
close OUT;

logger "$0 done."
