library(ggplot2)
library(gridExtra)

dat = read.table(
  file = "diffs/TP.dataset.tool.taxo.nbseq.csv" ,
  sep = '\t',
  header = TRUE,
  comment.char = "@"

)
dat.corr = dat[which(dat$SUPERKINGDOM != "ERROR"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "REFERENCE"),]
#head(dat)

#-- display superkingdom accordiong to proportion of TP
plot1 <- ggplot(aes(y = PERC.GOOD, x = CLASS), data = dat.corr) +
  geom_violin(col = "#444444", fill="#444444") +
  geom_jitter(
    height = 0,
    width = 0.3,
    data = dat.corr,
    aes(y = PERC.GOOD, x = CLASS, col = TOOL),
    size = 0.2
  ) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90,  vjust = 0.5, hjust=1)) +
  labs(x = "Superkingdom",
       y = "Proportion of true postives",
       col = "Software",
       size = "Number of\nsequences") +
  theme(legend.position = "top")+ 
  coord_flip()

plot1

height = length(levels(dat.corr$CLASS)) * 10

ggsave(
  plot = plot1,
  filename = "TAXONOMY/TP.class.tool.pdf",
  units="mm",
  width = 250,
  height = height,
  limitsize = F
)


ggsave(
  plot = plot1,
  filename = "TAXONOMY/TP.superkingdom.tool.svg",
  width = 8,
  height = 5
)
ggsave(
  plot = plot1,
  filename = "TAXONOMY/TP.superkingdom.tool.png",
  width = 8,
  height = 5
)



#-- display phylum according to proportion of TP
# split in 2 groups according to
dat.corr = dat[which(dat$PHYLUM != "ERROR"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "REFERENCE"),]


#-- display tools according to proportion of TP per superkingdom
dat.corr = dat[which(dat$SUPERKINGDOM != "ERROR"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "REFERENCE"),]

dat.corr = dat.corr[which(dat.corr$TOOL != "MYCC"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "BINSANITY"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "BINSANITY.WF"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "MAXBIN20"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "METAWATT"),]

dat.corr.cam = dat.corr[which(dat.corr$DATASET != "CAMI1h"),]

dat.corr$group = 2

lv = levels(dat.corr$PHYLUM)
dat.corr[which(dat.corr$PHYLUM %in% lv[1:round((length(lv)+0.5)/2)]),]$group = 1

p1 <- ggplot(aes(y = PERC.GOOD, x = PHYLUM), data = subset(dat.corr, dat.corr$group == 1)) +
  geom_jitter(
    height = 0,
    width = 0.3,
    data = subset(dat.corr, dat.corr$group == 1),
    aes(y = PERC.GOOD, x = PHYLUM, col = TOOL),
    size = 0.1
  ) +
  geom_violin(col = "darkgrey") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90,  vjust = 0.5, hjust=1)) +
  labs(x = "",
       y = "Proportion of true postives per phylum",
       col = "Software",
       size = "Number of\nsequences") +
  theme(legend.position = "top")


p2 <- ggplot(aes(y = PERC.GOOD, x = PHYLUM), data = subset(dat.corr, dat.corr$group == 2)) +
  geom_jitter(
    height = 0,
    width = 0.3,
    data = subset(dat.corr, dat.corr$group == 2),
    aes(y = PERC.GOOD, x = PHYLUM, col = TOOL),
    size = 0.1
  ) +
  geom_violin(col = "darkgrey") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90,  vjust = 0.5, hjust=1)) +
  labs(x = "Phylum",
       y = "Proportion of true postives",
       col = "Software",
       size = "Number of\nsequences") +
  theme(legend.position = "bottom")

plot2 <- arrangeGrob(p1, p2, ncol = 1, nrow = 2)
plot(plot2)

ggsave(file="TAXONOMY/TP.phylum.tool.svg", plot2, units="cm", width = 15, height = 20)
ggsave(file="TAXONOMY/TP.phylum.tool.png", plot2, units="cm", width = 15, height = 20)


#-- display tools according to proportion of TP per superkingdom
dat.corr = dat[which(dat$SUPERKINGDOM != "ERROR"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "REFERENCE"),]

dat.corr = dat.corr[which(dat.corr$TOOL != "MYCC"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "BINSANITY"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "BINSANITY.WF"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "MAXBIN20"),]
dat.corr = dat.corr[which(dat.corr$TOOL != "METAWATT"),]

dat.corr.cam = dat.corr[which(dat.corr$DATASET != "CAMI1h"),]


plot3 <- ggplot() +
  geom_violin(aes(y = PERC.GOOD, x = TOOL), data = dat.corr, col = "darkgrey") +
  geom_jitter(
    height = 0,
    width = 0.1,
    data = dat.corr,
    aes(y = PERC.GOOD, x = TOOL,  col = PHYLUM),
    size = 0.5
  ) +
  theme_bw() +
  labs(x = "Binning software", y = "Proportion of true postives", col = "Superkingdom") +
  theme(axis.text.x = element_text(angle = 0, hjust = 1, vjust = 0.5)) +
  theme(legend.position = "bottom") + coord_flip()

plot3

ggsave(
  plot = plot3,
  filename = "/home/kgravouil/MANUSCRIT/article01/bmc_bioinformatics_submission/FIG2__base.pdf",
  units="mm",
  width = 170,
  height = 1000
)



ggsave(
  plot = plot3,
  filename = "TAXONOMY/TP.tool.superkingdom.svg",
  width = 8,
  height = 5
)
ggsave(
  plot = plot3,
  filename = "TAXONOMY/TP.tool.superkingdom.png",
  width = 8,
  height = 5
)



#-- display tools according to proportion of TP per superkingdom

dat <- dat[which(dat$DATASET != "CAMI1h"),]
my_tools = c("COCACOLA", "CONCOCT",  "DAS_TOOL", "MAXBIN22",
             "METABAT2.W.ABD", "METABAT.W.ABD",
             "METABAT2", "METABAT", "METAPROB")

dat <- dat[dat$TOOL %in% my_tools,]

dat$COVERAGE=0

for (m in unique(dat$DATASET)) {
  cov = read.table(paste("coverage/",m,".cov.csv", sep=""), dec=",", header = TRUE)
  
  for(i in 1:nrow(cov)) {
    taxid <- cov[i,1]
    coverage <- cov[i,2]
    if (nrow(dat[which(dat$DATASET==m & dat$TAXID==taxid),]) != 0)
      dat[which(dat$DATASET==m & dat$TAXID==taxid),]$COVERAGE = coverage
  }
  
}


df = data.frame(
  taxlevel=character(),
  taxon=character(),
  max=double(), 
  min=double(),
  avg=double(),
  cov=double(),
  Phylum=character(),
  stringsAsFactors=FALSE)

for (taxlevel in colnames(dat)[11]) {
  dat.corr = dat[ which(dat[[ taxlevel ]] != "ERROR"), ]
  for (taxon in levels(factor(dat.corr[[taxlevel]]))) {
    tutu = dat.corr[which(dat.corr[[taxlevel]]==taxon), ]
    # print(sprintf("%s;%s;%.2f;%.2f", taxlevel, taxon, max(tutu$PERC.GOOD), min(tutu$PERC.GOOD)))
    df[nrow(df) + 1,] = list(taxlevel,
                             taxon,
                             max(tutu$PERC.GOOD),
                             min(tutu$PERC.GOOD),
                             mean(tutu$PERC.GOOD),
                             mean(tutu$COVERAGE),
                             as.character(unique(tutu$PHYLUM)[1])
    )
  }
}

max(df$min)
min(df$max)

df[which(df$cov<1e-8),]$cov=1e-8
df[which(df$cov>=100),]$cov=100


### base.points

plot4max <- ggplot() +
  geom_hline(yintercept=0.50, alpha=0.5, size=0.5, color="red") +
  geom_hline(yintercept=0.70, alpha=0.5, size=0.5, color="orange") +
  geom_hline(yintercept=0.90, alpha=0.5, size=0.5, color="green") +
  geom_point(data=df, aes(x=cov, y=max, label=taxon, color=Phylum)) +
  theme_minimal() +
  # xlim(c(0,100)) +
  ylim(c(-0,1)) +
  labs(title="Figure 4",
       x = "Average coverage",
       y = "Maximum proportion of true postives") +
  theme(legend.position="none")

plot4max

ggsave(
  plot = plot4max,
  filename = "/home/kgravouil/MANUSCRIT/article01/peerj_submission/Figures/_FIG4__base.max.points.svg",
  units="mm",
  width = 170,
  height = 120
)


plot4avg <- ggplot() +
  geom_hline(yintercept=0.50, alpha=0.5, size=0.5, color="red") +
  geom_hline(yintercept=0.70, alpha=0.5, size=0.5, color="orange") +
  geom_hline(yintercept=0.90, alpha=0.5, size=0.5, color="green") +
  geom_point(data=df, aes(x=cov, y=avg, label=taxon, color=Phylum)) +
  theme_minimal() +
  # xlim(c(0,100)) +
  ylim(c(-0,1)) +
  labs(title="Figure 4",
       x = "Average coverage",
       y = "Average proportion of true postives") +
  theme(legend.position="none")

plot4avg

ggsave(
  plot = plot4avg,
  filename = "/home/kgravouil/MANUSCRIT/article01/peerj_submission/Figures/_FIG4__base.avg.points.svg",
  units="mm",
  width = 170,
  height = 120
)

### base.text

plot5max <- ggplot() +
  geom_hline(yintercept=0.50, alpha=0.5, size=0.5, color="red") +
  geom_hline(yintercept=0.70, alpha=0.5, size=0.5, color="orange") +
  geom_hline(yintercept=0.90, alpha=0.5, size=0.5, color="green") +
  geom_text(data=df, aes(x=cov, y=max, label=taxon, color=Phylum)) +
  theme_minimal() +
  xlim(c(0,110)) +
  ylim(c(-0,1)) +
  labs(title="Figure 4",
       x = "Average coverage",
       y = "Maximum proportion of true postives") +
  theme(legend.position="none")

plot5max

ggsave(
  plot = plot5max,
  filename = "/home/kgravouil/MANUSCRIT/article01/peerj_submission/Figures/_FIG4__base.max.text.svg",
  units="mm",
  width = 170,
  height = 120
)


plot5avg <- ggplot() +
  geom_hline(yintercept=0.50, alpha=0.5, size=0.5, color="red") +
  geom_hline(yintercept=0.70, alpha=0.5, size=0.5, color="orange") +
  geom_hline(yintercept=0.90, alpha=0.5, size=0.5, color="green") +
  geom_text(data=df, aes(x=cov, y=avg, label=taxon, color=Phylum)) +
  theme_minimal() +
  # xlim(c(0,100)) +
  ylim(c(-0,1)) +
  labs(title="Figure 4",
       x = "Average coverage",
       y = "Average proportion of true postives") +
  theme(legend.position="none")

plot5avg

ggsave(
  plot = plot5avg,
  filename = "/home/kgravouil/MANUSCRIT/article01/peerj_submission/Figures/_FIG4__base.avg.text.svg",
  units="mm",
  width = 170,
  height = 120
)

###


plot7 <- ggplot(data = df, aes(y=avg, x=reorder(taxon, -avg))) +
  geom_hline(yintercept=0.50, size=0.75, color="red") +
  geom_hline(yintercept=0.70, color="orange") +
  geom_hline(yintercept=0.90, color="green") +
  theme_minimal() +
  theme(
    # axis.title.y=element_blank(),
    # axis.title.x=element_blank(),
    # axis.text.x=element_blank(),
    # axis.ticks.x=element_blank(),
    axis.text.x = element_text(angle = 90, hjust = 1),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_line( size=.25, color="#999999" )) +
  labs(x = "Genus", y = "Proportion of true postives") +
  geom_point(size=1, shape=3)

plot7


ggsave(
  plot = plot7,
  filename = "/home/kgravouil/MANUSCRIT/article01/bmc_bioinformatics_submission/FIG2__base.svg",
  units="mm",
  width = 85,
  height = 60
)




# quit(save = "yes", status = 0)


# dat <- dat[dat$SUPERKINGDOM=="Bacteria",] # bacteria only

tool = "CONCOCT"
dataset = "M"
dim(dat[dat$SUPERKINGDOM == "Bacteria",]) # bacteria only


dat.tool <- dat#[dat$TOOL==tool,] # bacteria only
dat.tool <- dat[dat$DATASET == dataset,] # bacteria only
#-- display tools according to proportion of TF per phyla
ggplot(aes(y = PERC.GOOD, x = PHYLUM), data = dat.tool) +
  scale_shape_manual(values = c("S", "M", "L")) +
  # ggtitle(paste("Proportion of True Positives per phylum","-",tool)) +
  geom_violin(col = "grey") +
  # stat_summary(fun.data=data_summary,geom="crossbar",width=0.6, color="#999999") +
  geom_jitter(
    height = 0,
    width = 0.2,
    data = dat.tool,
    aes(
      y = PERC.GOOD,
      x = PHYLUM,
      col = TOOL,
      shape = DATASET
    ),
    size = 2.5
  ) +
  theme_bw() +
  labs(x = "Phylum",
       y = "Proportion of true postives",
       col = "Software",
       shape = "Binning software") +
  theme(axis.text.x = element_text(angle = 90,  vjust = 0.5, hjust=1)) +
  coord_flip() +
  theme(legend.position = "bottom")

#
# # some stats per phylum
# quantile.per.phylum=aggregate(PERC.GOOD ~ SUPERKINGDOM, data = dat, quantile)
# par(las=2)
# plot(quantile.per.phylum)
#
# quantile.per.phylum
# quantile.per.phylum$PERC.GOOD[,5] # max
# quantile.per.phylum$PERC.GOOD[,1] # min
#
# graphics.off()

nrow(dat)
for (n in colnames(dat)) {
  cat(n, length(unique(dat[[n]])), "\n")
}
