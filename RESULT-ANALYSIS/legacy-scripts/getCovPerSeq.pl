#!/usr/bin/env perl

use strict;
use warnings;
use 5.10.0;
use IO::Uncompress::Gunzip qw($GunzipError);
use File::Basename;
use List::MoreUtils qw(uniq);
use Data::Dumper::Simple;

# my $reads = "DATA-SIMULATION/genomes.S/S.PE-reads.fastq.gz";
# my $refgenomes = "DATA-SIMULATION/genomes.S/reference/S.ALL.GENOMES.fasta.gz";

my ($reads, $refgenomes) = @ARGV;
my $ref_folder = dirname($refgenomes)."/ref_genomes/";


#-- get IDS per file
my $f = {}; # $f->{ID} = FILENAME
foreach my $file (<$ref_folder/*>) {
  my $l = `grep "^>" $file`;
  chomp $l;
  my @lines = split("\n", $l);
  foreach (@lines) {
     my ($id) = split(/\s/, $_);
     $id =~ s/^>//;
     $f->{$id} = basename($file);
  }
}

# foreach my $x (keys $f) {
#   say $x, "\t", $f->{$x};
# }
# exit;

#-- get number of bases per file (from raw reads)
open ( my $gunzip_stream, "-|", "gzip -dc $reads") or die $!;

my $h = {};
my ($header, $id, $seq);
while ( <$gunzip_stream> ) {
  chomp;
  # last if $. > 300000;
  if ($. %4 == 1) {
    (undef, $id) = split(/\s/, $_);
    $id =~ s/reference=//;
    $id = $f->{$id}; #getFileFromID($id, $ref_folder);
  }
  elsif ($. % 4 == 2) {
    $seq = $_;
    $h->{$id} += length($seq);
  }
}
close $gunzip_stream;

# # print Dumper $h;
# say "#seqid\tnb_base";
# foreach my $x (keys $h) {
#   say $x, "\t", $h->{$x};
# }
# exit;

#-- get number of bases per sequence (from reference genomes)
open ( my $gunzip_stream, "-|", "gzip -dc $refgenomes") or die $!;

my $g = {};
my ($header, $id, $seq) = ("", "", "");
my $i = 0;
{
  local $/ = ">";
  while (<$gunzip_stream>) {
    next if ($. == 1); # skip first '>'
    my @s;
    ($header, @s) = split(/\n/, $_) ;
    ($id) = split(/\s/, $header);
    my $fn = $f->{$id};#getFileFromID($id, $ref_folder);
    $g->{$fn} += length(join('', @s));
  }
}
close $gunzip_stream;

# print Dumper $g;
# say "#seqid\tnb_base";
# foreach my $x (keys $g) {
#   say $x, "\t", $g->{$x};
# }
# exit;



# --- print final results
say "#filename\tnb_base_ref\tnb_base_seq\tcoverage";
foreach my $x (sort(uniq(values $f))) {
  say(
    join(
      "\t",
      $x, # FILENAME
      ( $g->{$x} or -1 ), # NUMBER OF BASE IN GENOME
      ( $h->{$x} or 0 ), # NUMBER OF SEQUENCED BASES
      sprintf("%2.4f", ( ( $h->{$x} or 0 ) / ( $g->{$x} or -1 ))) # AVERAGE COVERAGE
    )
  );
}



# get file name from sequence ID
sub getFileFromID {
  my ($id, $ref_folder) = @_;
  my $fn = `grep $id $ref_folder/*.fna | cut -d':' -f1` or warn $!;
  chop $fn;
  $fn = basename($fn);
  return($fn);
}


__END__

for D in $(echo S M L); do
  echo "# --- $D ---"

  perl scripts/getCovPerSeq.pl  \
    DATA-SIMULATION/genomes.${D}/${D}.PE-reads.fastq.gz  \
    DATA-SIMULATION/genomes.${D}/reference/${D}.ALL.GENOMES.fasta.gz  \
    > ${D}.coverage.tsv

done
