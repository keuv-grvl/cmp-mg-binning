#!/usr/bin/env bash

INBAM=$1
REFDIR=$2

which genomeCoverageBed
test -f "$INBAM"
# test -d "$REFDIR"

# for INREF in $(find "$REFDIR" -type f); do
#     genomeCoverageBed -bga -split  \
#       -ibam "$INBAM" -g "$INREF"  \
#       > "$(basename $INREF).cov"
# done

samtools depth $INBAM > "$(basename $INBAM).cov"



for F in $(find -type f -name "*.fna"); do
    LEN=$(grep -v "^>" $F | tr -d '\n'|wc -c)
    echo -e "$(basename $F)\t$LEN"
done > file2genomesize


# ./getCovPerGenome.sh  \
#   ../MAPPINGS/READS-on-REF_GENOMES/results/bowtie2.S.very-sensitive/S.S.ALL.GENOMES.sort.bam  \
#   ../DATA-SIMULATION/genomes.S/reference/ref_genomes/


# ./getCovPerGenome.sh  \
#   ../MAPPINGS/READS-on-REF_GENOMES/results/bowtie2.M.very-sensitive/M.M.ALL.GENOMES.sort.bam  \
#   ../DATA-SIMULATION/genomes.M/reference/ref_genomes/


# ./getCovPerGenome.sh  \
#   ../MAPPINGS/READS-on-REF_GENOMES/results/bowtie2.L.very-sensitive/L.L.ALL.GENOMES.sort.bam  \
#   ../DATA-SIMULATION/genomes.L/reference/ref_genomes/
