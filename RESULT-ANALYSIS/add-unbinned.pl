#!/usr/bin/env perl

use strict;

# use warnings;
use autodie;
use 5.10.0;
use File::Basename;

my $USAGE  = "usage: $0 <*.scaffold-ids.txt> <*.bins.tsv>\n";
my $IDFILE = shift @ARGV or die $USAGE;
my $INFILE = shift @ARGV or die $USAGE;
my $HEADER = "#name\tbin";

my $h = {};

sub logger {
    my ( $sec, $min, $hour, $mday, $mon, $year ) = localtime();
    printf(
        "[%04d-%02d-%02d %02d:%02d:%02d] %s\n",
        $year + 1900,
        $mon + 1, $mday, $hour, $min, $sec, join( " - ", @_ )
    );
}

#-- read data ------------------------------------------------------------------
say STDERR "#-- $IDFILE";
say STDERR "# loading IDS";
open( ID, "<", $IDFILE );
while ( my $id = <ID> ) {
    chomp $id;
    $h->{$id} = "UNBINNED";    # Init each sequence as "UNBINNED"
}
close ID;

say STDERR "# loading clusters";
open( IN, "<", $INFILE );
while (<IN>) {
    next if m/^#/;
    chomp;
    my ( $scaffold, $cluster ) = split /\t/;
    $h->{$scaffold}
        = $cluster;    # Override "UNBINNED" for sequence which have a cluster
}
close IN;

#-- print data -----------------------------------------------------------------
say STDERR "# printing data";

open( OUT, '>', "data-with-unbinned/" . basename($INFILE) );
say OUT $HEADER;
foreach ( keys( %{$h} ) ) {
    say OUT$_, "\t", $h->{$_};
}
close OUT;
