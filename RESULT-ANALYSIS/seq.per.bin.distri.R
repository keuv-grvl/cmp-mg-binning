setwd("/home/kgravouil/THESE/data/COMPARATIF-BINNING/RESULT-ANALYSIS")

library(ggplot2)
library(cowplot)


plots = c()
filenames <- c("bin.distri/seq.per.bin.distri.S.csv", "bin.distri/seq.per.bin.distri.M.csv", "bin.distri/seq.per.bin.distri.L.csv")

#-- S
experim <- unlist(strsplit(filenames[1], "[.]"))[6]
dat <- read.delim(filenames[1], header = TRUE, sep = "\t", quote = "\"", dec = ",")
dat$Software <- dat$tool

plots[[experim]] <- ggplot(dat, aes(cluster, nb, color=Software)) + 
  geom_line() +
  ggtitle("Distribution of sequences per binning software") +
  theme_minimal() +
  scale_y_log10(breaks=c(1,2,5,
                         10,25,50,
                         100,250,500,
                         1000,2500,5000)) +
  theme(panel.border = element_blank(), 
        panel.grid.major.x = element_blank(),
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())+
  ylab("Number of sequences") +
  xlab("Clusters (ordered by size)") 

#-- M
experim <- unlist(strsplit(filenames[2], "[.]"))[6]
dat <- read.delim(filenames[2], header = TRUE, sep = "\t", quote = "\"", dec = ",")
dat$Software <- dat$tool

plots[[experim]] <- ggplot(dat, aes(cluster, nb, color=Software)) + 
  geom_line() +
  theme_minimal() +
  scale_y_log10(breaks=c(1,2,5,
                         10,25,50,
                         100,250,500,
                         1000,2500,5000)) +
  theme(panel.border = element_blank(), 
        panel.grid.major.x = element_blank(),
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()) +
  ylab("Number of sequences") +
  xlab("Clusters (ordered by size)") 

#-- L
experim <- unlist(strsplit(filenames[3], "[.]"))[6]
dat <- read.delim(filenames[3], header = TRUE, sep = "\t", quote = "\"", dec = ",")
dat$Software <- dat$tool

plots[[experim]] <- ggplot(dat, aes(cluster, nb, color=Software)) + 
  geom_line() +
  theme_minimal() +
  scale_y_log10(breaks=c(1,2,5,
                         10,25,50,
                         100,250,500,
                         1000,2500,5000)) +
  theme(panel.border = element_blank(), 
        panel.grid.major.x = element_blank(),
        panel.grid.minor = element_blank(),
        axis.line = element_line(colour = "black"),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()) +
  ylab("Number of sequences") +
  xlab("Clusters (ordered by size)") 


#-- all
p.all <- plot_grid(plots$S, plots$M, plots$L, labels=c("S", "M", "L"), ncol = 1, nrow = 3)
p.all

# ggsave(plot = p.all, filename = "bin.distri/nbseq.per.bin.profiles.svg", width = 5, height = 6)
# ggsave(plot = p.all, filename = "bin.distri/nbseq.per.bin.profiles.png", width = 5, height = 6)
