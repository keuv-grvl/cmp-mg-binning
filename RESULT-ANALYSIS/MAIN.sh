#!/usr/bin/env bash

### usage:
# PURGE=1 ./MAIN.sh
# WITH_UNBINNED=0 ./MAIN.sh
# SKIP_FORMAT=1 ./MAIN.sh


# enable auto logging
mkdir -p log
exec > >(tee -a -i "log/logfile-$(date +"%Y%m%d-%H%M%S").txt")
exec 2>&1

set -euo pipefail
# set -x

which Rscript
which perl


logger () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")]" "$@"
}

WD="$HOME/data/cmp-mg-binning/RESULT-ANALYSIS"

DATADIR=$WD/data
ASSOCDIR=$WD/assoc
STATDIR=$WD/stats

: "${PURGE:=0}"
: "${SKIP_FORMAT:=0}"
: "${WITH_UNBINNED:=1}"
: "${PACKAGE_RESULTS:=1}"
: "${FMT_BINNING:=0}"

[ "$PURGE" -ne 0 ] \
  && echo "PURGING" \
  && rm -rf "$DATADIR" "$ASSOCDIR" "$STATDIR" data-with-unbinned heatmaps diffs TAXONOMY PCOA *.scaffold-ids.txt stats-all.* _* tmp.* \
  && exit 42 ;  # this is not considered a normal exit yet we can identify it's been OK by test the return code

cd "$WD"
mkdir -p "$DATADIR"
mkdir -p "$ASSOCDIR"
mkdir -p "$STATDIR"
mkdir -p heatmaps

#--- Format binner outputs ----------------------------------------------------#
logger "Format binner outputs"
[ "$SKIP_FORMAT" -ne 0 ] || bash format-outputs.sh

#-- add 'UNBINNED'
logger "Add 'UNBINNED' to binner outputs"
[ "$WITH_UNBINNED" -ne 0 ] \
  && DATADIR=$WD/data-with-unbinned \
  && mkdir -p "$DATADIR" \
  && for F in data/*; do
    FF=$(basename "$F" | cut -d'.' -f1)
    perl add-unbinned.pl "${FF}.scaffold-ids.txt" "$F" 2>/dev/null &
  done

wait

#-- format as bioboxes binning files
if [ "$FMT_BINNING" -ne 0 ] ; then
  for TSVFILE in $DATADIR/*.tsv; do
    ./tsv2binning $TSVFILE "$(basename $TSVFILE)"
  done
fi


#--- Associations -------------------------------------------------------------#
logger "Search for associations"

for E in $(echo S M L CAMI1h); do
  REF="$DATADIR/${E}.reference.tsv"
  for F in $DATADIR/${E}.*.tsv; do
    FF=$(basename "$F" .tsv)
    Rscript "$WD/classes-clusters-association.R" "$REF" "$F"  \
      > "$ASSOCDIR/$FF.csv" 2> "$STATDIR/$FF.tsv" &
  done
done

wait


#--- summarize stats ----------------------------------------------------------#
logger "Summarize stats"

# usage: join_rec file1.tsv file2.tsv file3.tsv file*.tsv
# do not try to join 23k files...
function join_rec {
  f1=$1; f2=$2;
  shift 2;
  if [ $# -gt 0 ]; then
    join -a1 -a2 -e 0 -o auto -t$'\t' "$f1" "$f2" | join_rec - "$@" ;
  else
    join -a1 -a2 -e 0 -o auto -t$'\t' "$f1" "$f2" ;
  fi
}


# join stat files then transpose it
join_rec $STATDIR/* | \
  awk 'BEGIN { FS=OFS="\t" }
  {
      for (rowNr=1;rowNr<=NF;rowNr++) {
          cell[rowNr,NR] = $rowNr
      }
      maxRows = (NF > maxRows ? NF : maxRows)
      maxCols = NR
  }
  END {
      for (rowNr=1;rowNr<=maxRows;rowNr++) {
          for (colNr=1;colNr<=maxCols;colNr++) {
              printf "%s%s", cell[rowNr,colNr], (colNr < maxCols ? OFS : ORS)
          }
      }
  }' > tmp.stats-all.csv

echo -e "dataset\ttool" > tmp.info.csv
cat "tmp.stats-all.csv"  \
  | cut -f1  \
  | tail -n+2  \
  | sed 's/\.bins.tsv//'  \
  | sed 's/\.tsv//'  \
  | sed 's/\./\t/' >> tmp.info.csv

paste tmp.stats-all.csv tmp.info.csv > stats-all.csv
# rm tmp.info.csv tmp.stats-all.csv

logger "Plotting results"
Rscript plot-binning-metrics.R stats-all.csv


#--- check binning results according to taxonomy ------------------------------#
logger "Checking taxonomy"
mkdir -p diffs TAXONOMY
for C in "S" "M" "L" "CAMI1h"; do
  perl check-taxo.pl "$C" # compare binning against reference & add taxonomic lineage
  # Rscript check-taxo.R "$C"  # produces old and ugly boxplots
done;

# compute and plot True Positive proportions 
Rscript get_percgoodclassif.R  # produces 'diffs/TP.dataset.tool.taxo.nbseq.csv'
Rscript binning.results.per.phylum.R # produces violon plots in 'TAXONOMY/'


#--- evaluate consensus clustering --------------------------------------------#
logger "Evaluate complementarity"
mkdir -p PCOA
Rscript cmp.binner.pcoa.R 

[ "$PACKAGE_RESULTS" -ne 0 ] \
  && (
    PCKGDIR="RESULTS_WITHOUT_UNBINNED" ;
    [ "$WITH_UNBINNED" -ne 0 ] && PCKGDIR="RESULTS_WITH_UNBINNED" ;
    logger "Packaging restults tp '${PCKGDIR}'" ;
    mkdir -p $PCKGDIR ;
    mv "$DATADIR" "$ASSOCDIR" "$STATDIR" data-with-unbinned heatmaps diffs TAXONOMY PCOA *.scaffold-ids.txt stats-all.* _* tmp.* $PCKGDIR;
  )


logger  "Bye."
