#!/usr/bin/env bash

WD="$HOME/data/cmp-mg-binning"
# pushd $WD

HEADER="#name\tbin"
###output format
# scaffold-0.0{tab}NC_011831.1

OUTDIR="$WD/RESULT-ANALYSIS/data"
mkdir -p $OUTDIR

#- 1: get scaffolds ids
#- 2: which scaffold with which cluster
#- 3: save to 'data'

log () {
  echo "[$(date +"%Y-%m-%d %H:%M:%S")] $@"
}

log "Start"
log "Ouputs will be saved in '${OUTDIR}'"


#--- Get scaffold ids ---------------------------------------------------------#

#from:
# $WD/CUT-UP-SCAFFOLDS/L.Scaffolds.1kb+.split10kb.fasta
# $WD/CUT-UP-SCAFFOLDS/M.Scaffolds.1kb+.split10kb.fasta
# $WD/CUT-UP-SCAFFOLDS/S.Scaffolds.1kb+.split10kb.fasta

for E in $(echo S M L); do
  F=$(readlink -f $WD/CUT-UP-SCAFFOLDS/${E}*.fasta)
  grep "^>" $F | tr -d '>' > $WD/RESULT-ANALYSIS/${E}.scaffold-ids.txt
done &

grep -v "^#" $WD/BINNING/CAMI_formatted_results/CAMI1h.reference.tsv | \
  cut -f1 > $WD/RESULT-ANALYSIS/CAMI1h.scaffold-ids.txt &

#--- Get bin ids --------------------------------------------------------------#

### bwa
log "Extract bins from BWA"
for E in $(echo S M L) ; do
	echo -e "$HEADER" > $OUTDIR/${E}.reference.tsv
  samtools view -F2048 "$WD/BINNING/bwa/output/${E}.ALL.GENOMES.bam" \
    | cut -f1,3  \
    >> $OUTDIR/${E}.reference.tsv
done &


### metabat
log "Extract bins from MetaBAT"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.metabat.bins.tsv
  for F in $(ls $WD/BINNING/metabat/output/${E}/* | sort -n) ; do
    FF=$(basename $F .fa)
    FF=$(echo $FF | sed "s/${E}.//")

    for K in $(grep "^>" $F | sed 's/>//g') ; do
      echo -e "$K\t$FF" >> $OUTDIR/${E}.metabat.bins.tsv
    done
  done
done &


### metabat w/ abd
log "Extract bins from MetaBAT (using abd)"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.metabat.w.abd.bins.tsv
  for F in $(ls $WD/BINNING/metabat-with-abd/output/${E}/*.fa) ; do
    FF=$(basename $F .fa)
    FF=$(echo $FF | sed -r "s/^.+\.//")
    for K in $(grep "^>" $F | sed 's/>//g') ; do
      echo -e "$K\t$FF" >> $OUTDIR/${E}.metabat.w.abd.bins.tsv
    done
  done
done &


### concoct
log "Extract bins from CONCOCT"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.concoct.bins.tsv
  F="$WD/BINNING/concoct/output/${E}_clustering_gt1000.csv"
  cat "$WD/BINNING/concoct/output/${E}_clustering_gt1000.csv"  \
  | sed 's/,/\t/'  \
  | sed 's/bin//'  \
  >> $OUTDIR/${E}.concoct.bins.tsv
done  &


### maxbin
log "Extract bins from MaxBin"
for E in $(echo S M L); do 
  echo -e "$HEADER" > $OUTDIR/${E}.maxbin22.bins.tsv
  for F in $(ls $WD/BINNING/maxbin/${E}.output/${E}.*.fasta) ; do
    FF=$(basename $F .fasta)
    FF=$(echo $FF | sed "s/${E}.//")
    for K in $(grep "^>" $F | sed 's/>//g') ; do
      echo -e "$K\t$FF" >> $OUTDIR/${E}.maxbin22.bins.tsv
    done
  done
done &


### metaprob
log "Extract bins from MetaProb"
for E in $(echo S M L) ; do
  echo -e "$HEADER" > $OUTDIR/${E}.metaprob.bins.tsv
  cat $WD/BINNING/metaprob/output/${E}.bins/${E}.Scaffolds.1kb+.split10kb.fasta.clusters.csv  \
  | sed 's/>//'  \
  | sed 's/,/\t/'  \
  >> $OUTDIR/${E}.metaprob.bins.tsv
done &


### cocacola
log "Extract bins from COCACOLA"
# $WD/BINNING/cocacola/S.output/clustering_aggOptSep_full_link_beta_10000.csv
# $WD/BINNING/cocacola/M.output/clustering_aggOptSep_full_link_beta_5000.csv
# $WD/BINNING/cocacola/L.output/clustering_aggOptSep_full_link_beta_1000.csv

if true; then
  echo -e "$HEADER" > $OUTDIR/S.cocacola.bins.tsv
  cat $WD/BINNING/cocacola/S.output/clustering_aggOptSep_full_link_beta_10000.csv \
    | tr ',' '\t'  \
    >> $OUTDIR/S.cocacola.bins.tsv

  echo -e "$HEADER" > $OUTDIR/M.cocacola.bins.tsv
  cat $WD/BINNING/cocacola/M.output/clustering_aggOptSep_full_link_beta_5000.csv \
    | tr ',' '\t'  \
    >> $OUTDIR/M.cocacola.bins.tsv

  echo -e "$HEADER" > $OUTDIR/L.cocacola.bins.tsv
  cat $WD/BINNING/cocacola/L.output/clustering_aggOptSep_full_link_beta_1000.csv \
    | tr ',' '\t'  \
    >> $OUTDIR/L.cocacola.bins.tsv
fi &

### metabat2
log "Extract bins from MetaBAT2"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.metabat2.bins.tsv
  for F in $(ls $WD/BINNING/metabat2/output/${E}/* | sort -n) ; do
    FF=$(basename $F .fa)
    FF=$(echo $FF | sed "s/${E}.//")
    for K in $(grep "^>" $F | sed 's/>//g') ; do
      echo -e "$K\t$FF" >> $OUTDIR/${E}.metabat2.bins.tsv
    done
  done
done &


### metabat2-with-abd
log "Extract bins from MetaBAT2 (using abd)"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.metabat2.w.abd.bins.tsv
  for F in $(ls $WD/BINNING/metabat/output/${E}/* | sort -n) ; do
    FF=$(basename $F .fa)
    FF=$(echo $FF | sed "s/${E}.//")
    for K in $(grep "^>" $F | sed 's/>//g') ; do
      echo -e "$K\t$FF" >> $OUTDIR/${E}.metabat2.w.abd.bins.tsv
    done
  done
done &


### das_tool
log "Extract bins from DAS_Tool"
for E in $(echo S M L); do
  echo -e "$HEADER" > $OUTDIR/${E}.das_tool.bins.tsv
  cat $WD/BINNING/das_tool/output/${E}/${E}_DASTool_scaffolds2bin.txt >> $OUTDIR/${E}.das_tool.bins.tsv
done &


### CAMI results
log "Collect bins from CAMI/AMBER"
# convert binning file to tsv
B2T="$WD/RESULT-ANALYSIS/binning2tsv"
for F in $WD/BINNING/CAMI_formatted_results/CAMI1h.*.tsv; do
  cp $F $OUTDIR
done &

wait

log "$0 done."
