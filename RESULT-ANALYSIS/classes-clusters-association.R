#!/usr/bin/env Rscript

#--- manage arguments ---------
classes.file  <- "RESULT-ANALYSIS/data-with-unbinned/S.reference.tsv"
clusters.file <- "RESULT-ANALYSIS/data-with-unbinned/S.cocacola.bins.tsv"
clusters.file <- "RESULT-ANALYSIS/data-with-unbinned/S.das_tool.bins.tsv"

# classes.file  <- "RESULT-ANALYSIS/data/S.reference.tsv"
# clusters.file <- "RESULT-ANALYSIS/data/S.concoct.bins.tsv"
# clusters.file <- "RESULT-ANALYSIS/data/S.das_tool.bins.tsv"


if (!interactive()) {
  args <- commandArgs(trailingOnly = TRUE)
  if (length(args)!=2) {
    print("usage: command <reference_classes> <clusters>")
    quit()
  }
  
  classes.file  <- args[1]
  clusters.file <- args[2]
  
  if(!file.exists(classes.file) || !file.exists(clusters.file)) {
    print("ERROR: file does not exist")
    quit()
  }
  
}
#--- functions ------------
system.err.println <- function (...) {
  write(paste(...), stderr())
}

system.out.println <- function (...) {
  write(paste(...), stdout())
}

import_data <- function (filename, header = TRUE, sep = "\t", quote = "\"", dec = ",") {
  dat <- read.delim(filename, header = TRUE, sep = "\t", quote = "\"", dec = ",")
  dat <- dat[,1:2]
  colnames(dat) <- c("E", "C")
  dat$E <- as.character(dat$E)
  dat$C <- as.character(dat$C)
  return(dat)
}

nameOfMax <- function (x, plot = F) {
  if (plot) {
    plot(sort(x), type="o", ylim = c(0,1),  xaxt='n')
    axis(1, at = 1:length(x), labels=names(x), las=2)
  }
  
  return(names(sort(x))[length(x)])
}

multimax <- function (x, threshold = 0.5, plot = F) {
  #TODO ne renvoyer que le max (sans tenir compte du seuil)
  if (plot) {
    plot(sort(x),  type="o", ylim = c(0,1))
    abline(a=threshold, b=0, col="red")
  }
  # system.err.println(paste("Number of multimax:", length(x[x>=threshold])))
  # si je n'ai rien sous mon seuil, je renvoie NA
  if((length(x[x>=threshold])) == 0)
    return("UNBINNED")
  return(names(sort(x))[length(x)])
  # si j'ai plusieurs égaux aux max, j'en renvois 1 nom au hasard
  if((length(x[x>=max(x)])) > 1)
    return(sample(names(which(x>=max(x))))[1])
  # sinon, je renvoie le nom du + abondant
  return(names(which(x>=threshold))[1])
}

get_metrics <- function(x) {
  # init
  metrics <- list()
  metrics$file <- basename(clusters.file)
  metrics$total <- sum(x)
  metrics$nclasse <- length(rownames(x))
  metrics$ncluster <- length(colnames(x))
  metrics$TP <- 0 # nb classé correctement (vrai classé comme vrai)
  metrics$FP <- 0 # nb classé incorrectement (faux classé comme vrai)
  metrics$FN <- 0 # nb pas classé incorrectement (vrai classé comme faux)
  metrics$TN <- 0 # nb pas classé correctement (faux classé comme faux)
  # compute TP, FP, TN, FN
  # pour chaque cluster
  for (i in 1:metrics$nclasse) {
    if (is.na(assoc[i])) {
      # ce cluster n'est associé à aucune classe, on passe
      next
    }
    c <- names(assoc[i])
    k <- assoc[i]
    # system.err.println(paste(i, c, k, sep = ":"))
    
    if(k != "UNBINNED") {
      metrics$TP <- metrics$TP + x[c,k]
      metrics$FP <- metrics$FP + (sum(x[c,]) - x[c,k])
      metrics$FN <- metrics$FN + (sum(x[,k]) - x[c,k])
      metrics$TN <- metrics$TN + (sum(x) - sum(x[,k]) - sum(x[c,]) + x[c,k])
    } else { # si 'UNBINNED', les TRUE sont comptabilisés comme des FALSE
      metrics$FP <- metrics$FP + x[c,k]
      metrics$FP <- metrics$FP + (sum(x[c,]) - x[c,k])
      metrics$FN <- metrics$FN + (sum(x[,k]) - x[c,k])
      metrics$FN <- metrics$FN + (sum(x) - sum(x[,k]) - sum(x[c,]) + x[c,k])
    }
    
  }
  # compute other metrics
  metrics$sensitivity <- metrics$TP / (metrics$TP+metrics$FN)
  metrics$specificity <- metrics$TN / (metrics$FP+metrics$TN)
  metrics$precision   <- metrics$TP / (metrics$TP+metrics$FP)
  metrics$recall      <- metrics$TP / (metrics$TP+metrics$FN) # = sensitivity
  metrics$FDR         <- metrics$FP / (metrics$FP+metrics$TN)
  metrics$accuracy    <- (metrics$TP+metrics$TN)/(metrics$TP+metrics$TN+metrics$FP+metrics$FN)
  metrics$F1score     <- (2*metrics$TP)/(2*metrics$TP+metrics$FP+metrics$FN)
  metrics$MCC         <- ((metrics$TP*metrics$TN)-(metrics$FP-metrics$FN)) / sqrt((metrics$TP+metrics$FP)*(metrics$TP+metrics$FN)*(metrics$TN+metrics$FP)*(metrics$TN+metrics$FN))
  metrics$FPR         <- (1 - metrics$specificity)
  metrics$TPR         <- metrics$sensitivity
  return(metrics)
}

print.metrics <- function(metr) {
  for (f in names(metr)) {
    system.err.println(paste(f, metr[[f]], sep = "\t"))
  }
}


#--- main ---------
classes     <- import_data(classes.file)
predictions <- import_data(clusters.file)

# check data format
if (! is.data.frame(classes))     stop("not a data frame")
if (! is.data.frame(predictions)) stop("not a data frame")
if ( is.null(classes$E))          stop("data frame 'classe' malformed")
if ( is.null(classes$C))          stop("data frame 'classe' malformed")
if ( is.null(predictions$E))      stop("data frame 'prediciton' malformed")
if ( is.null(predictions$C))      stop("data frame 'prediciton' malformed")

#### confusion matrix ##########################################################
# init named confusion matrix
n <- length(unique(classes$C))
m <- length(unique(predictions$C))
mat <- matrix(data = 0, ncol = m, nrow = n)
rownames(mat) <- c(unique(classes$C))
colnames(mat) <- c(unique(predictions$C))
# populate confusion matrix
for (c in unique(classes$C)) {
  elem_c <- classes$E[classes$C == c]
  for (k in unique(predictions$C)) {
    elem_k <- predictions$E[predictions$C == k]
    v <- length(intersect(elem_c, elem_k))
    # system.err.println(paste(paste(c, k, sep = " & "), v, sep = "  ->  "))
    mat[c, k] <- v
  }
}
# quantile(mat[mat>0])
# mat[which(mat<=quantile(mat[mat>0], probs=c(.25)))] <- 0 # on supprimer le "bruit", c-à-d si inférieur au 1er quartile
# length(mat[mat>0])
# for reading purpose
# mat.t <- t(mat)
mat.p <- t(apply(mat, 1, FUN=function(x) { x/sum(x) }))

# la classe X est composé à 97.3% de séquences provenant du cluster Y
mat.p[is.nan(mat.p)] <- 0

if(!interactive())
  png(paste("heatmaps/",basename(clusters.file),".png", sep=''),    # create PNG for the heat map
      width = 5*300,        # 5 x 300 pixels
      height = 5*300,
      res = 300,            # 300 pixels per inch
      pointsize = 8)        # smaller font size
# creates a own color palette from red to green
my_palette <- colorRampPalette(c("white", "blue"))(n = 200)


gplots::heatmap.2(
  x = mat.p,
  # main = basename(clusters.file),
  trace ="none",
  margins = c(9,9),
  dendrogram = "none",
  cexRow=0.5,cexCol=0.5,
  key = F,
  # Colv="NA", Rowv = "NA",
  col = my_palette,
  xlab = basename(classes.file),
  ylab = basename(clusters.file)
)
# write.table(mat.p, file='mat.p.tsv', quote=FALSE, sep='\t')
if(!interactive())
  graphics.off()

assoc <- apply(X = mat.p, MARGIN = 1, FUN = nameOfMax)

for (n in names(assoc)) {
  system.out.println(paste(n, assoc[[ n ]], sep=";"))
}

for (n in names(assoc)) {
  if(length(assoc[[ n ]]) == 2)
    system.err.println(paste(n, assoc[[ n ]], sep=";"))
}

#### metrics ###################################################################
metr <- get_metrics(mat)
# str(metr)
print.metrics(metr)

