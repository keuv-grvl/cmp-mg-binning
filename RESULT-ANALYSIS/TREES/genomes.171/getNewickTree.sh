#!/usr/bin/env bash

VENV="ete3-env"
source activate $VENV \
  || conda create -n ete3-env -c etetoolkit ete3 ete3_external_apps \
  && source activate $VENV

ete3 ncbiquery --tree --full_lineage --search $(cat M.taxid.txt | tr "\n" " ") > genomes.M.171.nwk

echo "Now import 'genomes.M.171.nwk' in http://itol.embl.de/"
echo "Then drag&drop 'itol_colors.txt' in the tree"

source deactivate $VENV
